package net.eloboost.suite.controller;

import java.awt.event.ActionEvent;

/**
 * Functional interface for "RESET" operation - handles reverting to default
 * configuration in exceptional cases.
 * 
 * @author Georgi Iliev
 */
@FunctionalInterface
public interface IResetConfiguration {

    /**
     * Resets the configuration fields in the view(UI) to their default values.
     * 
     * @param configurationReset
     *            - the button click event.
     */
    void resetConfiguration(ActionEvent configurationReset);
}
