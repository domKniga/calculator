package net.eloboost.suite.controller;

import java.awt.event.ActionEvent;

/**
 * Functional interface for "LOCK/UNLOCK" operation - handles enabling/disabling
 * modification of the fields.
 * 
 * @author Georgi Iliev
 *
 */
@FunctionalInterface
public interface IToggleComponentAccess {

    /**
     * Enables/disables the input in fields.
     * 
     * @param componentАccessChanged
     *            - the button click event.
     */
    void toggleAcessInCard(ActionEvent componentАccessChanged);
}
