package net.eloboost.suite.controller;

import java.awt.event.ActionEvent;

/**
 * Functional interface for "APPLY" operation - handles committing the changes
 * made to the configurations.
 * 
 * @author Georgi Iliev
 */
@FunctionalInterface
public interface IApplyConfiguration {

    /**
     * Applies the changed settings through the view(UI) and saves them.
     * 
     * @param configurationComitted
     *            - the button click event.
     */
    void applyConfiguration(ActionEvent configurationComitted);

}
