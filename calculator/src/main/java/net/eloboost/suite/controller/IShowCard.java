package net.eloboost.suite.controller;

import java.awt.event.ItemEvent;

/**
 * Functional interface for "SHOW" operation - handles navigation through
 * configuration cards.
 * 
 * @author Georgi Iliev
 */
@FunctionalInterface
public interface IShowCard {

    /**
     * Navigates to the chosen configuration card.
     * 
     * @param cardSelectionChanged
     *            - the item selection change event.
     */
    void showCard(ItemEvent cardSelectionChanged);
}
