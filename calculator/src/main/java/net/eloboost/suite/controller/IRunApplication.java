package net.eloboost.suite.controller;

import java.awt.event.ActionEvent;

/**
 * Functional interface for "RUN" operation - handles choosing the runtime mode
 * of the Eloboost suite.
 * 
 * @author Georgi Iliev
 */
@FunctionalInterface
public interface IRunApplication {

    /**
     * Runs the Eloboost suite in the appropriate mode, either Calculator or
     * Report.
     * 
     * @param applicationStarted
     *            - the button click event.
     */
    void runApplication(ActionEvent applicationStarted);
}
