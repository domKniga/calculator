package net.eloboost.suite.controller;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.event.MouseInputAdapter;

import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;

import net.eloboost.suite.exceptions.LookupFolderNotFoundException;
import net.eloboost.suite.model.AbstractSettings;
import net.eloboost.suite.model.EloboostModel;
import net.eloboost.suite.model.calculator.Boost;
import net.eloboost.suite.model.calculator.CalculatorSettings;
import net.eloboost.suite.model.calculator.Cash;
import net.eloboost.suite.model.enums.ConfigurationCards;
import net.eloboost.suite.model.enums.ReportScopes;
import net.eloboost.suite.model.enums.ReportTypes;
import net.eloboost.suite.model.enums.TargetMonths;
import net.eloboost.suite.model.report.ReportMetadata;
import net.eloboost.suite.model.report.ReportSettings;
import net.eloboost.suite.utils.EloboostFileService;
import net.eloboost.suite.utils.calculator.AbstractCashConverter;
import net.eloboost.suite.utils.calculator.EloboostPageProcessor;
import net.eloboost.suite.utils.calculator.IEloboostCashConverter;
import net.eloboost.suite.utils.calculator.IEloboostPageProcessor;
import net.eloboost.suite.utils.calculator.YahooCashConverter;
import net.eloboost.suite.utils.report.ExcellReportGenerator;
import net.eloboost.suite.utils.report.IEloboostReportGenerator;
import net.eloboost.suite.view.EloboostView;
import net.eloboost.suite.view.customizers.JodaDateModel;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;

/**
 * The Eloboost controller.
 * 
 * @author Georgi Iliev
 */
public class EloboostController implements IEloboostController {

    private static final Logger LOGGER = Logger.getLogger(EloboostController.class);

    private EloboostModel model;
    private EloboostView view;

    /**
     * @param model
     *            - the model.
     * @param view
     *            - the view.
     */
    public EloboostController(final EloboostModel model, final EloboostView view) {
	this.model = model;
	this.view = view;
    }

    @Override
    public void initializeData() {
	final Currency[] currencies = model.getCurrencies().toArray(new Currency[0]);
	view.getSourceCurrencyPicker().setModel(new DefaultComboBoxModel<Currency>(currencies));
	view.getTargetCurrencyPicker().setModel(new DefaultComboBoxModel<Currency>(currencies));

	view.getReportTypePicker()
		.setModel(new DefaultComboBoxModel<ReportTypes>(model.getReportTypes().toArray(new ReportTypes[0])));
	view.getReportScopePicker()
		.setModel(new DefaultComboBoxModel<ReportScopes>(model.getReportScopes().toArray(new ReportScopes[0])));
	view.getTargetMonthPicker()
		.setModel(new DefaultComboBoxModel<TargetMonths>(model.getTargetMonths().toArray(new TargetMonths[0])));

	CalculatorSettings calculatorSettings = (CalculatorSettings) model.getCalculatorConfiguration();
	view.getLookupDirField().setText(calculatorSettings.getLookupDir());
	view.getSourceCurrencyPicker().setSelectedItem(Currency.getInstance(calculatorSettings.getSourceCurrency()));
	view.getTargetCurrencyPicker().setSelectedItem(Currency.getInstance(calculatorSettings.getTargetCurrency()));
	((JodaDateModel) view.getTargetDatePicker().getModel()).setValue( // nl
		LocalDate.parse(calculatorSettings.getTargetDate(), // nl
			DateTimeFormat.forPattern(CalculatorSettings.TARGET_DATE_PATTERN)));
	view.getWakeUpTimeSpinner().getModel().setValue( // nl
		LocalDateTime.parse(calculatorSettings.getWakeUpTime(), // nl
			DateTimeFormat.forPattern(CalculatorSettings.WAKEUP_TIME_PATTERN)).toDate());

	ReportSettings reportSettings = (ReportSettings) model.getReportConfiguration();
	view.getOutputDirField().setText(reportSettings.getOutputDir());
	view.getReportTypePicker().setSelectedItem(ReportTypes.valueOf(reportSettings.getType()));
	view.getReportScopePicker().setSelectedItem(ReportScopes.valueOf(reportSettings.getScope()));
	view.getTargetMonthPicker().setSelectedItem(TargetMonths.valueOf(reportSettings.getTargetMonth()));
	view.getTargetYearSpinner().getModel().setValue(LocalDateTime
		.parse(reportSettings.getTargetYear(), DateTimeFormat.forPattern(ReportSettings.TARGET_YEAR_PATTERN))
		.toDate());

	String folderFieldTooltipHint = "Double-click to browse for folder";
	view.getLookupDirField()
		.setToolTipText(view.toMultilineTooltip(view.getLookupDirField().getText(), folderFieldTooltipHint));
	view.getOutputDirField()
		.setToolTipText(view.toMultilineTooltip(view.getOutputDirField().getText(), folderFieldTooltipHint));
    }

    @Override
    public void initializeActions() {
	view.getConfigurationCardPicker().addItemListener(cardSelectionChanged -> showCard(cardSelectionChanged));

	view.getUnlockButton().addActionListener(componentАccessChanged -> toggleAcessInCard(componentАccessChanged));
	view.getResetButton().addActionListener(configurationReset -> resetConfiguration(configurationReset));
	view.getApplyButton().addActionListener(configurationComitted -> applyConfiguration(configurationComitted));
	view.getRunButton().addActionListener(applicationStarted -> runApplication(applicationStarted));

	MouseInputAdapter browseFolderListener = new MouseInputAdapter() {
	    @Override
	    public void mouseClicked(MouseEvent fieldClicked) {
		if (fieldClicked.getClickCount() == 2) {
		    browseFolders(fieldClicked);
		}

		super.mouseClicked(fieldClicked);
	    }
	};

	view.getLookupDirField().addMouseListener(browseFolderListener);
	view.getOutputDirField().addMouseListener(browseFolderListener);

	MouseInputAdapter fieldAccessListener = new MouseInputAdapter() {
	    @Override
	    public void mouseClicked(MouseEvent fieldClicked) {
		if (fieldClicked.getClickCount() == 2) {
		    askForUnlock();
		}

		super.mouseClicked(fieldClicked);
	    }
	};

	view.getSourceCurrencyPicker().addMouseListener(fieldAccessListener);
	view.getTargetCurrencyPicker().addMouseListener(fieldAccessListener);
	view.getTargetDatePicker().getJFormattedTextField().addMouseListener(fieldAccessListener);
	((JSpinner.DefaultEditor) view.getWakeUpTimeSpinner().getEditor()).getTextField()
		.addMouseListener(fieldAccessListener);

	view.getReportTypePicker().addMouseListener(fieldAccessListener);
	view.getReportScopePicker().addMouseListener(fieldAccessListener);
	view.getTargetMonthPicker().addMouseListener(fieldAccessListener);
	((JSpinner.DefaultEditor) view.getTargetYearSpinner().getEditor()).getTextField()
		.addMouseListener(fieldAccessListener);
    }

    private void browseFolders(MouseEvent fieldClicked) {
	JTextField field = (JTextField) fieldClicked.getSource();

	if (field.isEditable()) {
	    JFileChooser folderChooser = new JFileChooser();
	    folderChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	    folderChooser.setCurrentDirectory(EloboostFileService.DESKTOP_FOLDER.toFile());

	    folderChooser.showOpenDialog(view);

	    File selectedFolder = folderChooser.getSelectedFile();
	    if (selectedFolder != null) {
		field.setText(selectedFolder.getPath());
	    }
	} else {
	    askForUnlock();
	}
    }

    private void askForUnlock() {
	int unlockSettings = JOptionPane.showConfirmDialog(view,
		"Cannot edit when settings are locked! Do you want to unlock them?", "CHOICE",
		JOptionPane.YES_NO_OPTION);

	if (unlockSettings == JOptionPane.YES_OPTION) {
	    view.getUnlockButton().doClick();
	}
    }

    @Override
    public void showCard(ItemEvent cardSelectionChanged) {
	final Container configurationPanel = view.getConfigurationPanel();
	final CardLayout cardManager = (CardLayout) (configurationPanel.getLayout());

	cardManager.show(configurationPanel, (String) cardSelectionChanged.getItem());
    }

    @Override
    public void toggleAcessInCard(ActionEvent componentАccessChanged) {
	final Container activeCard = determineSelectedCard(view.getConfigurationPanel());

	toggleAccessRecursively(activeCard);
    }

    private void toggleAccessRecursively(final Container configurationPanel) {
	Component[] childComponents = configurationPanel.getComponents();

	for (Component component : childComponents) {
	    if (component instanceof Container) {
		toggleAccessRecursively((Container) component);
	    }

	    if ((component instanceof JTextField)) {
		final JTextField textField = (JTextField) component;
		textField.setEditable(!textField.isEditable());
	    }

	    else if (component instanceof JComboBox) {
		component.setEnabled(!component.isEnabled());
	    }

	    else if (component instanceof JDatePickerImpl) {
		final JDatePickerImpl datePicker = (JDatePickerImpl) component;

		datePicker.setTextEditable(!datePicker.isTextEditable());

		// Disable date picker button
		final Component pickerButton = datePicker.getComponent(1);
		pickerButton.setEnabled(!pickerButton.isEnabled());

	    } else if (component instanceof JSpinner) {
		final JSpinner spinner = (JSpinner) component;

		// Keep text editor disabled(control only via arrows)
		((JSpinner.DateEditor) spinner.getEditor()).getTextField().setEditable(false);

		// Disable arrows
		final Component upArrow = spinner.getComponent(0);
		final Component downArrow = spinner.getComponent(1);

		upArrow.setEnabled(!upArrow.isEnabled());
		downArrow.setEnabled(!downArrow.isEnabled());
	    }
	}
    }

    @Override
    public void resetConfiguration(ActionEvent configurationReset) {
	final Container activeCard = determineSelectedCard(view.getConfigurationPanel());

	Optional<ConfigurationCards> configurationCard = view.getConfigurationCards().stream() // nl
		.filter(card -> Objects.equals(card.identifier(), activeCard.getName())) // nl
		.findFirst();
	configurationCard.ifPresent(card -> resetCard(card));
    }

    private void resetCard(final ConfigurationCards configurationCard) {
	switch (configurationCard) {
	case ReportCard:
	    view.getOutputDirField().setText(EloboostFileService.REPORTS_FOLDER.toString());
	    view.getOutputDirField().setToolTipText(view.getOutputDirField().getText());
	    view.getReportTypePicker().setSelectedItem(ReportTypes.valueOf(ReportSettings.DEFAULT_REPORT_TYPE));
	    view.getReportScopePicker().setSelectedItem(ReportScopes.valueOf(ReportSettings.DEFAULT_REPORT_SCOPE));
	    view.getTargetMonthPicker().setSelectedItem(TargetMonths.valueOf(ReportSettings.DEFAULT_TARGET_MONTH));
	    view.getTargetYearSpinner().getModel().setValue(LocalDateTime.parse(ReportSettings.DEFAULT_TARGET_YEAR,
		    DateTimeFormat.forPattern(ReportSettings.TARGET_YEAR_PATTERN)).toDate());
	    break;
	case CalculatorCard:
	default:
	    view.getLookupDirField().setText(EloboostFileService.DESKTOP_FOLDER.toString());
	    view.getLookupDirField().setToolTipText(view.getLookupDirField().getText());
	    view.getSourceCurrencyPicker()
		    .setSelectedItem(Currency.getInstance(CalculatorSettings.DEFAULT_SOURCE_CURRENCY));
	    view.getTargetCurrencyPicker()
		    .setSelectedItem(Currency.getInstance(CalculatorSettings.DEFAULT_TARGET_CURRENCY));
	    view.getTargetDatePicker().getJFormattedTextField().setText(CalculatorSettings.DEFAULT_TARGET_DATE);
	    ((JSpinner.DateEditor) view.getWakeUpTimeSpinner().getEditor()).getTextField()
		    .setText(CalculatorSettings.DEFAULT_WAKEUP_TIME);
	    break;
	}
    }

    @Override
    public void applyConfiguration(ActionEvent configurationComitted) {
	final Container activeCard = determineSelectedCard(view.getConfigurationPanel());

	Optional<ConfigurationCards> configurationCard = view.getConfigurationCards().stream() // nl
		.filter(card -> Objects.equals(card.identifier(), activeCard.getName())) // nl
		.findFirst();
	configurationCard.ifPresent(card -> editConfiguration(card));

	return;
    }

    private void editConfiguration(final ConfigurationCards configurationCard) {
	Map<String, String> newSettings = new HashMap<>();

	switch (configurationCard) {
	case ReportCard:
	    newSettings.put(ReportSettings.KEY_OUTPUT_DIR, view.getOutputDirField().getText());
	    newSettings.put(ReportSettings.KEY_TYPE,
		    ((ReportTypes) view.getReportTypePicker().getSelectedItem()).toString());
	    newSettings.put(ReportSettings.KEY_SCOPE,
		    ((ReportScopes) view.getReportScopePicker().getSelectedItem()).toString());
	    newSettings.put(ReportSettings.KEY_TARGET_MONTH,
		    ((TargetMonths) view.getTargetMonthPicker().getSelectedItem()).toString());
	    newSettings.put(ReportSettings.KEY_TARGET_YEAR,
		    ((JSpinner.DateEditor) view.getTargetYearSpinner().getEditor()).getTextField().getText());
	    break;
	case CalculatorCard:
	    newSettings.put(CalculatorSettings.KEY_LOOKUP_DIR, view.getLookupDirField().getText());
	    newSettings.put(CalculatorSettings.KEY_SOURCE_CURRENCY,
		    ((Currency) view.getSourceCurrencyPicker().getSelectedItem()).getCurrencyCode());
	    newSettings.put(CalculatorSettings.KEY_TARGET_CURRENCY,
		    ((Currency) view.getTargetCurrencyPicker().getSelectedItem()).getCurrencyCode());
	    newSettings.put(CalculatorSettings.KEY_TARGET_DATE,
		    view.getTargetDatePicker().getJFormattedTextField().getText());
	    newSettings.put(CalculatorSettings.KEY_WAKEUP_TIME,
		    ((JSpinner.DateEditor) view.getWakeUpTimeSpinner().getEditor()).getTextField().getText());
	default:
	    break;
	}

	if (newSettings.values().contains(null) || newSettings.values().contains("")) {
	    JOptionPane.showMessageDialog(view, "Cannot submit empty field!", "ERROR", JOptionPane.ERROR_MESSAGE);
	    return;
	}

	AbstractSettings.get(configurationCard).ifPresent(settings -> settings.edit(newSettings));
    }

    @Override
    public void runApplication(ActionEvent applicationStarted) {
	final Container activeCard = determineSelectedCard(view.getConfigurationPanel());

	Optional<ConfigurationCards> configurationCard = view.getConfigurationCards().stream() // nl
		.filter(card -> Objects.equals(card.identifier(), activeCard.getName())) // nl
		.findFirst();
	configurationCard.ifPresent(card -> runInMode(card));
    }

    private Container determineSelectedCard(final Container configurationPanel) {
	Container selectedCard = null;

	for (Component component : configurationPanel.getComponents()) {
	    if (component.isVisible() == true) {
		selectedCard = (Container) component;
	    }
	}

	return selectedCard;
    }

    private void runInMode(final ConfigurationCards configurationCard) {
	switch (configurationCard) {
	case ReportCard:
	    ReportTypes reportType = (ReportTypes) view.getReportTypePicker().getSelectedItem();
	    ReportScopes reportScope = (ReportScopes) view.getReportScopePicker().getSelectedItem();
	    TargetMonths targetMonth = (TargetMonths) view.getTargetMonthPicker().getSelectedItem();
	    String targetYear = ((JSpinner.DateEditor) view.getTargetYearSpinner().getEditor()).getTextField()
		    .getText();

	    ReportMetadata reportMetadata = new ReportMetadata(reportType, reportScope, targetMonth, targetYear);

	    IEloboostReportGenerator reportGenerator = new ExcellReportGenerator(reportMetadata);
	    try {
		Path outputFolder = Paths.get(view.getOutputDirField().getText());

		reportGenerator.generateReport(outputFolder);
	    } catch (IOException ioe) {
		String errorMessage = "Report generation failed with metadata: " + reportMetadata.toString();

		LOGGER.error(errorMessage, ioe);
		JOptionPane.showMessageDialog(view, errorMessage + "\n Cause:" + ioe.getMessage(), "ERROR",
			JOptionPane.ERROR_MESSAGE);
	    }

	    break;
	case CalculatorCard:
	    try {
		Path lookupFolder = Paths.get(view.getLookupDirField().getText());

		Set<String> eloboostPages = EloboostFileService.loadEloboostPages(lookupFolder);
		if (eloboostPages.isEmpty()) {
		    String noPagesMsg = "No pages available in specified lookup folder:["
			    + EloboostFileService.PARENT_FOLDER.toString() + "]";

		    LOGGER.error(noPagesMsg);
		    JOptionPane.showMessageDialog(view, noPagesMsg, "WARNING", JOptionPane.WARNING_MESSAGE);
		    break;
		}

		double totalCashRawValue = 0.0d;

		String sourceCurrency = ((Currency) view.getSourceCurrencyPicker().getSelectedItem()).getCurrencyCode();
		String targetCurrency = ((Currency) view.getTargetCurrencyPicker().getSelectedItem()).getCurrencyCode();

		IEloboostPageProcessor pageProcessor;
		IEloboostCashConverter converter = new YahooCashConverter(AbstractCashConverter.DEFAULT_ATTEMPTS_CAP);

		Cash originalCash = new Cash(sourceCurrency);
		Cash convertedCash = new Cash(targetCurrency);

		LocalDate targetDate = LocalDate.parse( // nl
			view.getTargetDatePicker().getJFormattedTextField().getText(), // nl
			DateTimeFormat.forPattern(CalculatorSettings.TARGET_DATE_PATTERN) // nl
		);
		LocalTime wakeUpTime = LocalTime.parse( // nl
			((JSpinner.DateEditor) view.getWakeUpTimeSpinner().getEditor()).getTextField().getText(), // nl
			DateTimeFormat.forPattern(CalculatorSettings.WAKEUP_TIME_PATTERN) // nl
		);

		Boost boost = new Boost(targetDate, wakeUpTime);

		for (String page : eloboostPages) {
		    pageProcessor = new EloboostPageProcessor(page, boost);

		    totalCashRawValue += pageProcessor.processEloboostPage();
		}

		if (totalCashRawValue > 0.0d) {
		    originalCash = new Cash(totalCashRawValue, originalCash.getCurrency(), converter);
		    convertedCash = originalCash.convertTo(targetCurrency);
		}

		BigDecimal originalCashFinanceValue = originalCash.getFinanceValue();
		BigDecimal convertedCashFinanceValue = convertedCash.getFinanceValue();

		LOGGER.info("-------------------------------------------------------------");
		LOGGER.info(sourceCurrency + ": " + originalCashFinanceValue);
		LOGGER.info(targetCurrency + ": " + convertedCashFinanceValue);
		LOGGER.info("-------------------------------------------------------------");

		LocalDate boostDate = boost.date();
		EloboostFileService.appendLedger(boostDate, originalCashFinanceValue, convertedCashFinanceValue);

		String boostReport = "DAY: " + boostDate // nl
			+ "\n" + sourceCurrency + ": " + originalCashFinanceValue + "€" // nl
			+ "\n" + targetCurrency + ": " + convertedCashFinanceValue + "лв.";
		JOptionPane.showMessageDialog(view, boostReport, "BOOST REPORT", JOptionPane.INFORMATION_MESSAGE);
	    } catch (IOException ioe) {
		LOGGER.error(ioe.getMessage(), ioe);

		JOptionPane.showMessageDialog(view, "Problem accessing file: " + ioe.getMessage(), "ERROR",
			JOptionPane.ERROR_MESSAGE);
	    } catch (LookupFolderNotFoundException lfnfe) {
		LOGGER.error(lfnfe);

		JOptionPane.showMessageDialog(null, lfnfe.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
	    }
	default:
	    break;
	}

	continueOrQuit();
    }

    private void continueOrQuit() {
	final int choice = JOptionPane.showConfirmDialog(view, "Do you want to quit?", "CHOICE",
		JOptionPane.YES_NO_OPTION);
	if (choice == JOptionPane.YES_OPTION) {
	    view.dispatchEvent(new WindowEvent((JFrame) view, WindowEvent.WINDOW_CLOSING));
	}
    }

}
