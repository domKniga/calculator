package net.eloboost.suite.controller;

/**
 * Blueprint for an Eloboost controller.
 * 
 * @author Georgi Iliev
 */
public interface IEloboostController
	extends IShowCard, IToggleComponentAccess, IResetConfiguration, IApplyConfiguration, IRunApplication {

    /**
     * Initializes the default values of fields in the view(UI).
     */
    void initializeData();

    /**
     * Initializes the actions of the components in the view(UI).
     */
    void initializeActions();
}
