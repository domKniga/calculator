package net.eloboost.suite.view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.Currency;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.LookAndFeel;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

import org.apache.log4j.Logger;

import net.eloboost.suite.model.calculator.CalculatorSettings;
import net.eloboost.suite.model.enums.ConfigurationCards;
import net.eloboost.suite.model.enums.ReportScopes;
import net.eloboost.suite.model.enums.ReportTypes;
import net.eloboost.suite.model.enums.TargetMonths;
import net.eloboost.suite.model.report.ReportSettings;
import net.eloboost.suite.utils.EloboostFileService;
import net.eloboost.suite.view.customizers.JodaDateFormatter;
import net.eloboost.suite.view.customizers.JodaDateModel;
import net.eloboost.suite.view.styles.AbstractGUIPreset;
import net.sourceforge.jdatepicker.JDateComponentFactory;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;

/**
 * The Eloboost view(UI).
 * 
 * @author Georgi Iliev
 */
public class EloboostView extends JFrame {

    private static final long serialVersionUID = 6467226595868494027L;

    private static final Logger LOGGER = Logger.getLogger(EloboostView.class);

    private static final String LOGO_ICON = "appIcon.png";
    private static final String RESET_BTN_ICON = "resetBtnIcon.png";
    private static final String UNLOCK_BTN_ICON = "unlockBtnIcon.png";
    private static final String APPLY_BTN_ICON = "applyBtnIcon.png";
    private static final String RUN_BTN_ICON = "runBtnIcon.png";

    private List<ConfigurationCards> configurationCards;

    private AbstractGUIPreset preset;

    private JPanel configurationPanel;
    private JPanel controlPanel;

    private JPanel calculatorCard;
    private JPanel reportCard;

    private JComboBox<String> configurationCardPicker;

    private JTextField lookupDirField;
    private JComboBox<Currency> sourceCurrencyPicker;
    private JComboBox<Currency> targetCurrencyPicker;
    private JDatePickerImpl targetDatePicker;
    private JSpinner wakeUpTimeSpinner;

    private JTextField outputDirField;
    private JComboBox<ReportTypes> reportTypePicker;
    private JComboBox<ReportScopes> reportScopePicker;
    private JComboBox<TargetMonths> targetMonthPicker;
    private JSpinner targetYearSpinner;

    private JButton resetButton;
    private JButton unlockButton;
    private JButton applyButton;
    private JButton runButton;

    /**
     * @param preset
     *            - the preset.
     */
    public EloboostView(final AbstractGUIPreset preset) {
	super("Eloboost Suite");
	this.preset = preset;
	this.configurationCards = Collections.unmodifiableList(Arrays.asList(ConfigurationCards.values()));

	setLookAndFeel();
	this.preset.style(this);
	this.setIconImage(getImageResource(LOGO_ICON));

	addNavigationPanel();
	addConfigurationPanel();
	addControlPanel();
    }

    private void setLookAndFeel() {
	UIManager.LookAndFeelInfo iLAFs[] = UIManager.getInstalledLookAndFeels();

	for (int i = 0; i < iLAFs.length; i++) {
	    if (iLAFs[i].getName().equals("Nimbus")) {

		LookAndFeel nimbus = new NimbusLookAndFeel();
		nimbus.getDefaults().put("ToolTip.font", new FontUIResource("Rockwell", Font.BOLD, 16));

		try {
		    UIManager.setLookAndFeel(nimbus);
		} catch (UnsupportedLookAndFeelException e) {
		    LOGGER.error("Problem setting look & feel! Defaulting to Metal LAF...", e);
		    break;
		}

		return;
	    }
	}
    }

    /**
     * Sets the default button for the frame and displays it.
     */
    public void display() {
	SwingUtilities.getRootPane(runButton).setDefaultButton(runButton);

	this.pack();
	this.setLocationRelativeTo(null);
	this.setVisible(true);
    }

    private void addNavigationPanel() {
	configurationCardPicker = new JComboBox<String>();
	configurationCardPicker.setEnabled(true);
	preset.style(configurationCardPicker);

	// Add cards in picker
	configurationCards.forEach(card -> configurationCardPicker.addItem(card.identifier()));

	final int standardPadding = 5;

	GridBagConstraints constraints = new GridBagConstraints();
	constraints.insets = new Insets(standardPadding, standardPadding, standardPadding, standardPadding);
	constraints.ipadx = standardPadding;
	constraints.ipady = standardPadding;
	constraints.fill = GridBagConstraints.BOTH;
	constraints.weightx = 2;
	constraints.weighty = 1;

	final JPanel navigationPane = new JPanel(new GridBagLayout());
	navigationPane.setOpaque(false);

	navigationPane.add(configurationCardPicker, constraints);

	this.getContentPane().add(navigationPane, BorderLayout.PAGE_START);
    }

    private void addConfigurationPanel() {
	configurationPanel = new JPanel(new CardLayout());
	configurationPanel.setOpaque(true);
	configurationPanel.setBackground(Color.ORANGE);

	addCalculatorCard();
	addReportCard();

	this.getContentPane().add(configurationPanel, BorderLayout.CENTER);
    }

    private void addCalculatorCard() {
	calculatorCard = new JPanel();
	calculatorCard.setName(ConfigurationCards.CalculatorCard.identifier());
	calculatorCard.setLayout(new BoxLayout(calculatorCard, BoxLayout.Y_AXIS));
	calculatorCard.setOpaque(false);

	// Lookup directory
	final JLabel lookupDirLabel = new JLabel("Lookup folder:");
	preset.style(lookupDirLabel);

	lookupDirField = new JTextField();
	preset.style(lookupDirField);

	final JPanel lookupDirPane = new JPanel();
	preset.style(lookupDirPane);
	lookupDirPane.add(lookupDirLabel);
	lookupDirPane.add(lookupDirField);

	// Source currency
	final JLabel sourceCurrencyLabel = new JLabel("Currency(from):");
	preset.style(sourceCurrencyLabel);

	sourceCurrencyPicker = new JComboBox<Currency>();
	sourceCurrencyPicker.setEnabled(false);
	preset.style(sourceCurrencyPicker);

	final JPanel sourceCurrencyPane = new JPanel();
	preset.style(sourceCurrencyPane);
	sourceCurrencyPane.add(sourceCurrencyLabel);
	sourceCurrencyPane.add(sourceCurrencyPicker);

	// Target currency
	final JLabel targetCurrencyLabel = new JLabel("Currency(to):");
	preset.style(targetCurrencyLabel);

	targetCurrencyPicker = new JComboBox<Currency>();
	targetCurrencyPicker.setEnabled(false);
	preset.style(targetCurrencyPicker);

	final JPanel targetCurrencyPane = new JPanel();
	preset.style(targetCurrencyPane);
	targetCurrencyPane.add(targetCurrencyLabel);
	targetCurrencyPane.add(targetCurrencyPicker);

	// Target date
	final JLabel targetDateLabel = new JLabel("Target date:");
	preset.style(targetDateLabel);

	targetDatePicker = (JDatePickerImpl) JDateComponentFactory // nl
		.createJDatePicker(new JodaDateModel(), new JodaDateFormatter());
	preset.style(targetDatePicker);

	final JPanel targetDatePane = new JPanel();
	preset.style(targetDatePane);
	targetDatePane.add(targetDateLabel);
	targetDatePane.add(targetDatePicker);

	// Wake-up time
	final JLabel wakeUpTimeLabel = new JLabel("Wake-up time:");
	preset.style(wakeUpTimeLabel);

	wakeUpTimeSpinner = new JSpinner(new SpinnerDateModel());
	wakeUpTimeSpinner.setEditor(new JSpinner.DateEditor(wakeUpTimeSpinner, CalculatorSettings.WAKEUP_TIME_PATTERN));
	preset.style(wakeUpTimeSpinner);

	final JPanel wakeUpTimePane = new JPanel();
	preset.style(wakeUpTimePane);
	wakeUpTimePane.add(wakeUpTimeLabel);
	wakeUpTimePane.add(wakeUpTimeSpinner);

	// Stack(vertical) container
	final Box calculatorBox = Box.createVerticalBox();
	calculatorBox.add(lookupDirPane);
	calculatorBox.add(sourceCurrencyPane);
	calculatorBox.add(targetCurrencyPane);
	calculatorBox.add(targetDatePane);
	calculatorBox.add(wakeUpTimePane);

	final JScrollPane calculatorScroll = new JScrollPane(calculatorBox);
	preset.style(calculatorScroll);

	calculatorCard.add(calculatorScroll);
	configurationPanel.add(calculatorCard, ConfigurationCards.CalculatorCard.identifier());
    }

    private void addReportCard() {
	reportCard = new JPanel();
	reportCard.setName(ConfigurationCards.ReportCard.identifier());
	reportCard.setLayout(new BoxLayout(reportCard, BoxLayout.Y_AXIS));
	reportCard.setOpaque(false);

	// Output directory
	final JLabel outputDirLabel = new JLabel("Output folder:");
	preset.style(outputDirLabel);

	outputDirField = new JTextField();
	preset.style(outputDirField);

	final JPanel outputDirPane = new JPanel();
	preset.style(outputDirPane);
	outputDirPane.add(outputDirLabel);
	outputDirPane.add(outputDirField);

	// Report type
	final JLabel reportTypeLabel = new JLabel("Type:");
	preset.style(reportTypeLabel);

	reportTypePicker = new JComboBox<ReportTypes>();
	reportTypePicker.setEnabled(false);
	preset.style(reportTypePicker);

	final JPanel reportTypePane = new JPanel();
	preset.style(reportTypePane);
	reportTypePane.add(reportTypeLabel);
	reportTypePane.add(reportTypePicker);

	// Report scope
	final JLabel reportScopeLabel = new JLabel("Scope:");
	preset.style(reportScopeLabel);

	reportScopePicker = new JComboBox<ReportScopes>();
	reportScopePicker.setEnabled(false);
	preset.style(reportScopePicker);

	final JPanel reportScopePane = new JPanel();
	preset.style(reportScopePane);
	reportScopePane.add(reportScopeLabel);
	reportScopePane.add(reportScopePicker);

	// Target month
	final JLabel targetMonthLabel = new JLabel("Target month:");
	preset.style(targetMonthLabel);

	targetMonthPicker = new JComboBox<TargetMonths>();
	targetMonthPicker.setEnabled(false);
	preset.style(targetMonthPicker);

	final JPanel targetMonthPane = new JPanel();
	preset.style(targetMonthPane);
	targetMonthPane.add(targetMonthLabel);
	targetMonthPane.add(targetMonthPicker);

	// Target year
	final JLabel targetYearLabel = new JLabel("Target year:");
	preset.style(targetYearLabel);

	targetYearSpinner = new JSpinner(new SpinnerDateModel());
	targetYearSpinner.setEditor(new JSpinner.DateEditor(targetYearSpinner, ReportSettings.TARGET_YEAR_PATTERN));
	preset.style(targetYearSpinner);

	final JPanel targetYearPane = new JPanel();
	preset.style(targetYearPane);
	targetYearPane.add(targetYearLabel);
	targetYearPane.add(targetYearSpinner);

	// Stack(vertical) container
	final Box reportBox = Box.createVerticalBox();
	reportBox.add(outputDirPane);
	reportBox.add(reportScopePane);
	reportBox.add(reportTypePane);
	reportBox.add(targetMonthPane);
	reportBox.add(targetYearPane);

	final JScrollPane reportScroll = new JScrollPane(reportBox);
	preset.style(reportScroll);

	reportCard.add(reportScroll);
	configurationPanel.add(reportCard, ConfigurationCards.ReportCard.identifier());
    }

    private void addControlPanel() {
	controlPanel = new JPanel();
	preset.style(controlPanel);

	unlockButton = new JButton(new ImageIcon(getImageResource(UNLOCK_BTN_ICON)));
	preset.style(unlockButton);

	applyButton = new JButton(new ImageIcon(getImageResource(APPLY_BTN_ICON)));
	preset.style(applyButton);

	resetButton = new JButton(new ImageIcon(getImageResource(RESET_BTN_ICON)));
	preset.style(resetButton);

	runButton = new JButton(new ImageIcon(getImageResource(RUN_BTN_ICON)));
	preset.style(runButton);

	controlPanel.add(resetButton);
	controlPanel.add(unlockButton);
	controlPanel.add(applyButton);
	controlPanel.add(runButton);

	this.getContentPane().add(controlPanel, BorderLayout.PAGE_END);
    }

    private Image getImageResource(final String imageName) {
	return Toolkit.getDefaultToolkit()
		.getImage(Paths.get(EloboostFileService.IMG_FOLDER.toString(), imageName).toString());
    }

    /**
     * @param field
     *            - the field.
     * @param description
     *            - the description to add below the field text.
     * @return A multiline tooltip text.
     */
    public String toMultilineTooltip(final String fieldText, final String description) {
	StringBuilder multilineBuilder = new StringBuilder();

	multilineBuilder // nl
		.append("<html>") // nl
		.append("<i>" + fieldText + "</i>") // nl
		.append("<br/>") // nl
		.append("<b>(" + description + ")</b>") // nl
		.append("</html>");

	return multilineBuilder.toString();
    }

    public Container getConfigurationPanel() {
	return this.configurationPanel;
    }

    public JComboBox<String> getConfigurationCardPicker() {
	return this.configurationCardPicker;
    }

    public JButton getUnlockButton() {
	return this.unlockButton;
    }

    public JButton getResetButton() {
	return this.resetButton;
    }

    public JButton getApplyButton() {
	return this.applyButton;
    }

    public JButton getRunButton() {
	return this.runButton;
    }

    public JTextField getLookupDirField() {
	return this.lookupDirField;
    }

    public JComboBox<Currency> getSourceCurrencyPicker() {
	return this.sourceCurrencyPicker;
    }

    public JComboBox<Currency> getTargetCurrencyPicker() {
	return this.targetCurrencyPicker;
    }

    public JDatePickerImpl getTargetDatePicker() {
	return this.targetDatePicker;
    }

    public JSpinner getWakeUpTimeSpinner() {
	return this.wakeUpTimeSpinner;
    }

    public JTextField getOutputDirField() {
	return this.outputDirField;
    }

    public JComboBox<ReportTypes> getReportTypePicker() {
	return this.reportTypePicker;
    }

    public JComboBox<ReportScopes> getReportScopePicker() {
	return this.reportScopePicker;
    }

    public JComboBox<TargetMonths> getTargetMonthPicker() {
	return this.targetMonthPicker;
    }

    public JSpinner getTargetYearSpinner() {
	return this.targetYearSpinner;
    }

    public List<ConfigurationCards> getConfigurationCards() {
	return this.configurationCards;
    }
}
