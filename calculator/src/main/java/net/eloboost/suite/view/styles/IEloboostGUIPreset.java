package net.eloboost.suite.view.styles;

import java.awt.Component;

/**
 * Functional interface for preset - handles styling any given component.
 * 
 * @author Georgi Iliev
 */
@FunctionalInterface
public interface IEloboostGUIPreset {

    /**
     * Style the given component according to the application`s standards.
     * 
     * @param component
     *            - the component to be styled.
     */
    public void style(final Component component);

}
