package net.eloboost.suite.view.customizers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JFormattedTextField.AbstractFormatter;

/**
 * Custom date formatter for JODA time - used for JSpinner instances.
 * 
 * @author Georgi Iliev
 */
public class JodaDateFormatter extends AbstractFormatter {

    private static final long serialVersionUID = 8454034758984543034L;

    private String datePattern = "dd/MM/yyyy";
    private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);

    @Override
    public Object stringToValue(String text) throws ParseException {
	return dateFormatter.parseObject(text);
    }

    @Override
    public String valueToString(Object value) throws ParseException {
	if (value != null) {
	    Calendar calendar = (Calendar) value;
	    return dateFormatter.format(calendar.getTime());
	}

	return "";
    }

}
