package net.eloboost.suite.view.styles;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.text.DateFormatter;

import net.eloboost.suite.view.customizers.CustomComboBoxRenderer;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;

/**
 * Implementation for HD sized displays.
 * 
 * @author Georgi Iliev
 */
public class EloboostPreset extends AbstractGUIPreset {

    private static final int COMPONENT_FONTSIZE = 16;
    private static final String FONT_FAMILY = "Rockwell";

    public EloboostPreset() {
	valueLabelFont = new Font(FONT_FAMILY, Font.BOLD, COMPONENT_FONTSIZE);
	valueComponentFont = new Font(FONT_FAMILY, Font.BOLD + Font.ITALIC, COMPONENT_FONTSIZE);

	buttonHeight = 50;
	buttonWidth = 100;

	valueLabelWidth = 150;
	valueLabelHeight = 30;

	valueComponentWidth = 250;
	valueComponentHeight = 30;

	frameBackground = Color.BLACK;

	valueComponentForeground = Color.BLACK;
	valueComponentBackground = Color.WHITE;

	valueComponentSelectionForeground = Color.WHITE;
	valueComponentSelectionBackground = Color.BLACK;

	valueLabelForeground = Color.BLACK;
    }

    @Override
    protected void styleValueDisplayComponent(final JComponent component) {
	component.setPreferredSize(new Dimension(valueComponentWidth, valueComponentHeight));
	component.setFont(valueComponentFont);
    }

    @Override
    public void styleFrame(final JFrame frame) {
	frame.setResizable(false);
	frame.getContentPane().setLayout(new BorderLayout());
	frame.getContentPane().setBackground(frameBackground);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void styleHorizontalPanel(JPanel panel) {
	panel.setOpaque(false);
	panel.setLayout(new FlowLayout(FlowLayout.CENTER));
    }

    @Override
    public void styleTextField(final JTextField field) {
	styleValueDisplayComponent(field);

	field.setEditable(false);
	field.setForeground(valueComponentForeground);
	field.setBackground(valueComponentBackground);
	field.setSelectedTextColor(valueComponentSelectionForeground);
	field.setSelectionColor(valueComponentSelectionBackground);
    }

    @Override
    public <E> void styleComboBox(final JComboBox<E> comboBox) {
	styleValueDisplayComponent(comboBox);

	comboBox.setRenderer(new CustomComboBoxRenderer(valueComponentFont, valueComponentForeground,
		valueComponentBackground, valueComponentSelectionForeground, valueComponentSelectionBackground));
    }

    @Override
    public void styleLabel(final JLabel label) {
	label.setOpaque(false);
	label.setPreferredSize(new Dimension(valueLabelWidth, valueLabelHeight));
	label.setForeground(valueLabelForeground);
	label.setFont(valueLabelFont);
    }

    @Override
    public void styleButton(final JButton button) {
	button.setOpaque(false);
	button.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
    }

    @Override
    public void styleScrollPane(final JScrollPane scroll) {
	scroll.setOpaque(false);
	scroll.getViewport().setOpaque(false);
	scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
	scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    }

    @Override
    public void styleDatePicker(final JDatePickerImpl datePicker) {
	styleValueDisplayComponent(datePicker);
	datePicker.getJFormattedTextField().setFont(valueComponentFont);
	datePicker.getJFormattedTextField().setSelectedTextColor(valueComponentSelectionForeground);
	datePicker.getJFormattedTextField().setSelectionColor(valueComponentSelectionBackground);

	datePicker.setTextEditable(false);

	// Disable picker button
	datePicker.getComponent(1).setEnabled(false);
    }

    @Override
    public void styleSpinner(final JSpinner timeSpinner) {
	styleValueDisplayComponent(timeSpinner);

	JSpinner.DateEditor editor = (JSpinner.DateEditor) timeSpinner.getEditor();
	editor.getTextField().setEditable(false);

	DateFormatter formatter = (DateFormatter) editor.getTextField().getFormatter();
	formatter.setAllowsInvalid(false);
	formatter.setOverwriteMode(true);

	// Disable arrows
	timeSpinner.getComponent(0).setEnabled(false);
	timeSpinner.getComponent(1).setEnabled(false);
    }

}
