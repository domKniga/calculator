package net.eloboost.suite.view.styles;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;

import net.sourceforge.jdatepicker.impl.JDatePickerImpl;

/**
 * Abstract model of a GUI preset.
 * 
 * @author Georgi Iliev
 */
public abstract class AbstractGUIPreset implements IEloboostGUIPreset {

    protected int buttonHeight;
    protected int buttonWidth;
    protected int valueComponentWidth;
    protected int valueComponentHeight;
    protected int valueLabelWidth;
    protected int valueLabelHeight;

    protected Color frameBackground;
    protected Color valueLabelForeground;
    protected Color valueComponentForeground;
    protected Color valueComponentBackground;
    protected Color valueComponentSelectionForeground;
    protected Color valueComponentSelectionBackground;

    protected Font valueLabelFont;
    protected Font valueComponentFont;

    @Override
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void style(final Component component) {
	if (component instanceof JTextField) {
	    styleTextField((JTextField) component);
	} else if (component instanceof JLabel) {
	    styleLabel((JLabel) component);
	} else if (component instanceof JComboBox) {
	    styleComboBox((JComboBox) component);
	} else if (component instanceof JButton) {
	    styleButton((JButton) component);
	} else if (component instanceof JScrollPane) {
	    styleScrollPane((JScrollPane) component);
	} else if (component instanceof JDatePickerImpl) {
	    styleDatePicker((JDatePickerImpl) component);
	} else if (component instanceof JPanel) {
	    styleHorizontalPanel((JPanel) component);
	} else if (component instanceof JSpinner) {
	    styleSpinner((JSpinner) component);
	} else if (component instanceof JFrame) {
	    styleFrame((JFrame) component);
	}
    }

    /**
     * @param component
     *            - the value display component to be styled.
     */
    protected abstract void styleValueDisplayComponent(final JComponent component);

    /**
     * @param frame
     *            - the frame to be styled.
     */
    protected abstract void styleFrame(final JFrame frame);

    /**
     * @param panel
     *            - the panel to be styled.
     */
    protected abstract void styleHorizontalPanel(final JPanel panel);

    /**
     * @param field
     *            - the field to be styled.
     */
    protected abstract void styleTextField(final JTextField field);

    /**
     * @param comboBox
     *            - the combo box to be styled.
     */
    protected abstract <E> void styleComboBox(final JComboBox<E> comboBox);

    /**
     * @param label
     *            - the label to be styled.
     */
    protected abstract void styleLabel(final JLabel label);

    /**
     * @param button
     *            - the button to be styled.
     */
    protected abstract void styleButton(final JButton button);

    /**
     * @param scroll
     *            - the scroll pane to be styled.
     */
    protected abstract void styleScrollPane(final JScrollPane scroll);

    /**
     * @param datePicker
     *            - the jdatepicker to be styled.
     */
    protected abstract void styleDatePicker(final JDatePickerImpl datePicker);

    /**
     * @param spinner
     *            - the jspinner to be styled.
     */
    protected abstract void styleSpinner(final JSpinner spinner);

}
