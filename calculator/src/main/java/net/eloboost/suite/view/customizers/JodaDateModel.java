package net.eloboost.suite.view.customizers;

import java.util.Calendar;

import org.joda.time.LocalDate;

import net.sourceforge.jdatepicker.AbstractDateModel;

/**
 * Date model for JODA time date - used for JSpinner instances.
 * 
 * @author Georgi Iliev
 */
public class JodaDateModel extends AbstractDateModel<org.joda.time.LocalDate> {

    public JodaDateModel() {
	this(null);
    }

    /**
     * @param value
     *            - the date value.
     */
    public JodaDateModel(final LocalDate value) {
	super();
	setValue(value);
    }

    @Override
    protected LocalDate fromCalendar(Calendar from) {
	return new LocalDate(from.getTimeInMillis());
    }

    @Override
    protected Calendar toCalendar(LocalDate from) {
	Calendar to = Calendar.getInstance();
	to.setTime(from.toDate());
	return to;
    }

}
