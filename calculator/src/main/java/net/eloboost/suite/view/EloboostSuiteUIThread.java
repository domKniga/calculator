package net.eloboost.suite.view;

import java.util.Optional;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import net.eloboost.suite.controller.EloboostController;
import net.eloboost.suite.model.AbstractSettings;
import net.eloboost.suite.model.EloboostModel;
import net.eloboost.suite.model.calculator.CalculatorSettings;
import net.eloboost.suite.model.enums.ConfigurationCards;
import net.eloboost.suite.model.report.ReportSettings;
import net.eloboost.suite.view.styles.AbstractGUIPreset;
import net.eloboost.suite.view.styles.EloboostPreset;

/**
 * The UI Thread.
 * 
 * @author Georgi Iliev
 */
public class EloboostSuiteUIThread implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(EloboostSuiteUIThread.class);

    public final AbstractGUIPreset preset;

    public EloboostSuiteUIThread() {
	preset = new EloboostPreset();
    }

    @Override
    public void run() {
	final Optional<AbstractSettings> calculatorSettings = AbstractSettings.get(ConfigurationCards.CalculatorCard);
	final Optional<AbstractSettings> reportSettings = AbstractSettings.get(ConfigurationCards.ReportCard);

	if (calculatorSettings.isPresent() && reportSettings.isPresent()) {
	    final EloboostModel model = new EloboostModel( // nl
		    (CalculatorSettings) calculatorSettings.get(), // nl
		    (ReportSettings) reportSettings.get() // nl
	    );
	    final EloboostView view = new EloboostView(preset);
	    final EloboostController controller = new EloboostController(model, view);

	    controller.initializeData();
	    controller.initializeActions();

	    view.display();
	} else {
	    LOGGER.error("Configuration file missing!");
	    JOptionPane.showMessageDialog(null, "Configuration file missing!", "ERROR", JOptionPane.ERROR_MESSAGE);
	}
    }
}
