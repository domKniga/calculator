package net.eloboost.suite.model.calculator;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import net.eloboost.suite.exceptions.ColumnValueParseException;
import net.eloboost.suite.exceptions.InvalidTableColumnException;

/**
 * Representation of Eloboost 'Orders' data table.
 * 
 * @author Georgi Iliev
 */
public class OrdersTable extends AbstractDataTable {

    private static final Logger LOGGER = Logger.getLogger(OrdersTable.class);

    private static final String KEY_ORDER_ID = "orderId";
    private static final String KEY_ORDER_CASH = "orderCash";
    private static final String KEY_ORDER_DATE = "orderDate";

    /**
     * @param page
     *            - the page.
     * @param boost
     *            - the boost.
     */
    public OrdersTable(final Document page, final Boost boost) {
	super(page, boost);
	dataTableContainerElement = "div[id=cashlogs]";
	dataTableElement = "table[class=table table-bordered table-striped table-hover table-center]";
    }

    @Override
    protected void initializeDataTableColumns() {
	dataTableColumns.put(KEY_ORDER_ID, 1);
	dataTableColumns.put(KEY_ORDER_DATE, 0);
	dataTableColumns.put(KEY_ORDER_CASH, 3);
    }

    @Override
    public double profit() throws InvalidTableColumnException, ColumnValueParseException {
	int dateIndex = dataTableColumns.get(KEY_ORDER_DATE);
	int orderIndex = dataTableColumns.get(KEY_ORDER_ID);
	int cashIndex = dataTableColumns.get(KEY_ORDER_CASH);

	double orderCash = 0.0d;
	double totalOrderCash = 0.0d;

	Element orderDateElement = null;
	Element orderIdElement = null;
	Element orderCashElement = null;

	DateTime orderDate = null;

	Element ordersTableContainerElement = page.select(dataTableContainerElement).first();
	Element ordersTableElement = ordersTableContainerElement.select(dataTableElement).first();

	Elements rows = ordersTableElement.select("tr");
	for (int i = 1; i < rows.size(); i++) {
	    try {
		orderDateElement = rows.get(i).select("td").get(dateIndex);
		orderIdElement = rows.get(i).select("td").get(orderIndex);

		orderDate = DateTimeFormat.forPattern(ELOBOOST_DATE_PATTERN).parseDateTime(orderDateElement.text());
		orderDate = orderDate.withYear(2000 + orderDate.year().get());

		LOGGER.info("---------- ORDER: " + orderIdElement.text() + "----------");
		LOGGER.info("ORDER DATE: " + orderDate.toString(ELOBOOST_DATE_PATTERN));

		if (boost.isWithinBoostRun(orderDate)) {
		    orderCashElement = rows.get(i).select("td").get(cashIndex);
		    orderCash = Double.parseDouble(orderCashElement.text().replaceAll("[\\s€]", ""));
		    LOGGER.info("ORDER CASH: " + orderCash + " €");

		    totalOrderCash += orderCash;
		} else {
		    LOGGER.info("ORDER NOT WITHIN BOOST RUN!");
		}

		LOGGER.info("---------------------------------------------------------");
	    } catch (UnsupportedOperationException | IllegalArgumentException parseExc) {
		LOGGER.error(parseExc.getMessage(), parseExc);

		String orderDateTrace = getColumnValueTrace(orderDateElement);
		String gifCashTrace = getColumnValueTrace(orderCashElement);

		throw new ColumnValueParseException(orderDateTrace + gifCashTrace, 0);
	    } catch (RuntimeException re) {
		LOGGER.error(re.getMessage(), re);

		String orderIdTrace = getColumnValueTrace(orderIdElement);
		String orderDateTrace = getColumnValueTrace(orderDateElement);
		String gifCashTrace = getColumnValueTrace(orderCashElement);

		throw new InvalidTableColumnException(orderIdTrace + orderDateTrace + gifCashTrace);
	    }
	}

	return totalOrderCash;
    }

}
