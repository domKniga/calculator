package net.eloboost.suite.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import org.apache.log4j.Logger;

import net.eloboost.suite.model.calculator.CalculatorSettings;
import net.eloboost.suite.model.enums.ConfigurationCards;
import net.eloboost.suite.model.report.ReportSettings;
import net.eloboost.suite.utils.EloboostFileService;

/**
 * Abstract model of an Eloboost configuration.
 * 
 * @author Georgi Iliev
 */
public abstract class AbstractSettings implements IEloboostSuiteSettings {

    private static final Logger LOGGER = Logger.getLogger(AbstractSettings.class);

    protected String settingsFile;

    /**
     * @param settingsFile
     *            - the configuration file name with extension(.properties).
     */
    public AbstractSettings(final String settingsFile) {
	this.settingsFile = settingsFile;
	load();
    }

    /**
     * @param configurationCard
     *            - the configuration card.
     * @return The appropriate settings object for the specified configuration
     *         card.
     */
    public static Optional<AbstractSettings> get(final ConfigurationCards configurationCard) {
	AbstractSettings settings = null;

	switch (configurationCard) {
	case CalculatorCard:
	    settings = new CalculatorSettings("calculator.properties");
	    break;
	case ReportCard:
	    settings = new ReportSettings("report.properties");
	    break;
	default:
	    LOGGER.error("Unknown settings card!");
	    break;
	}

	return Optional.ofNullable(settings);
    }

    @Override
    public void edit(final Map<String, String> changedSettings) {
	try {
	    Properties settings = new Properties();

	    try (FileInputStream stream = new FileInputStream(
		    Paths.get(EloboostFileService.CONF_FOLDER.toString(), settingsFile).toFile());) {
		settings.load(stream);
	    } finally {
	    }

	    fallbackToDefaults(settings);

	    boolean settingsChanged = changedSettings.entrySet().stream() // nl
		    .anyMatch(entry -> !(settings.contains(entry.getValue())));

	    if (settingsChanged) {
		File outputFile = Paths.get(EloboostFileService.CONF_FOLDER.toString(), settingsFile).toFile();

		try (OutputStream outputFileAsStream = new FileOutputStream(outputFile);) {
		    LOGGER.info("BEFORE: " + settings);

		    settings.putAll(changedSettings);
		    settings.store(outputFileAsStream, "");

		    LOGGER.info("AFTER: " + settings);
		} finally {
		    LOGGER.warn("Modified settings!");
		}
	    }
	} catch (IOException ioe) {
	    LOGGER.error("Failed to edit configuration: ", ioe);
	}
    }

    public abstract void load();

    public abstract void fallbackToDefaults(final Properties settingsFile);

}
