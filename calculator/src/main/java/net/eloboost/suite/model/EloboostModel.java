package net.eloboost.suite.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import net.eloboost.suite.model.calculator.CalculatorSettings;
import net.eloboost.suite.model.enums.ReportScopes;
import net.eloboost.suite.model.enums.ReportTypes;
import net.eloboost.suite.model.enums.TargetMonths;
import net.eloboost.suite.model.report.ReportSettings;

/**
 * The Eloboost model.
 * 
 * @author Georgi Iliev
 */
public class EloboostModel {

    private CalculatorSettings calculatorConfiguration;
    private ReportSettings reportConfiguration;

    private List<Currency> currencies;
    private List<ReportTypes> reportTypes;
    private List<ReportScopes> reportScopes;
    private List<TargetMonths> targetMonths;

    /**
     * @param calculatorConfiguration
     *            - the calculator settings.
     * @param reportConfiguration
     *            - the report settings.
     */
    public EloboostModel(final CalculatorSettings calculatorConfiguration, final ReportSettings reportConfiguration) {
	this.calculatorConfiguration = calculatorConfiguration;
	this.reportConfiguration = reportConfiguration;

	this.currencies = Collections.unmodifiableList(loadCurrencies());
	this.targetMonths = Collections.unmodifiableList(Arrays.asList(TargetMonths.values()));
	this.reportTypes = Collections.unmodifiableList(Arrays.asList(ReportTypes.values()));
	this.reportScopes = Collections.unmodifiableList(Arrays.asList(ReportScopes.values()));
    }

    private List<Currency> loadCurrencies() {
	Set<Currency> currenciesInAlphaOrder = new TreeSet<Currency>(new Comparator<Currency>() {
	    @Override
	    public int compare(Currency c1, Currency c2) {
		return c1.getCurrencyCode().compareTo(c2.getCurrencyCode());
	    }
	});

	Locale[] locales = Locale.getAvailableLocales();
	for (Locale locale : locales) {
	    try {
		currenciesInAlphaOrder.add(Currency.getInstance(locale));
	    } catch (Exception e) {
		// Ignore inaccessible locales
	    }
	}

	return new ArrayList<Currency>(currenciesInAlphaOrder);
    }

    public List<Currency> getCurrencies() {
	return currencies;
    }

    public List<ReportTypes> getReportTypes() {
	return reportTypes;
    }

    public List<ReportScopes> getReportScopes() {
	return reportScopes;
    }

    public List<TargetMonths> getTargetMonths() {
	return targetMonths;
    }

    public CalculatorSettings getCalculatorConfiguration() {
	calculatorConfiguration.load();
	return calculatorConfiguration;
    }

    public ReportSettings getReportConfiguration() {
	reportConfiguration.load();
	return reportConfiguration;
    }

}
