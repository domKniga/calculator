package net.eloboost.suite.model;

import java.util.Map;
import java.util.Properties;

/**
 * Blueprint of an Eloboost configuration.
 * 
 * @author Georgi Iliev
 */
public interface IEloboostSuiteSettings {

    /**
     * Loads the current configuration. Never fails - worst case scenario loads
     * the default values.
     */
    void load();

    /**
     * Edit the configuration with the specified configuration items with new
     * values.
     * 
     * @param changedSettings
     *            - the configuration items map(with new values).
     */
    void edit(final Map<String, String> changedSettings);

    /**
     * Falls back to default value for any items(keys) missing a value.
     * 
     * @param settingsFile
     *            - the parsed settings file.
     */
    void fallbackToDefaults(final Properties settingsFile);

}
