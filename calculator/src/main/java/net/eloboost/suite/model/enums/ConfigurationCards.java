package net.eloboost.suite.model.enums;

/**
 * Enumeration of configuration cards.
 * 
 * @author Georgi Iliev
 */
public enum ConfigurationCards {

    /**
     * Calculator configuration card.
     */
    CalculatorCard("Calculator configuration", 0), // nl

    /**
     * Report configuration card.
     */
    ReportCard("Report configuration", 1);

    private Integer cardIndex;
    private String cardIdentifier;

    ConfigurationCards(final String cardIdentifier, final Integer cardIndex) {
	this.cardIdentifier = cardIdentifier;
	this.cardIndex = cardIndex;
    }

    public String identifier() {
	return this.cardIdentifier;
    }

    public Integer index() {
	return this.cardIndex;
    }
}
