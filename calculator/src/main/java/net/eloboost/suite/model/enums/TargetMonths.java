package net.eloboost.suite.model.enums;

/**
 * Enumeration for target months.
 * 
 * @author Georgi Iliev
 */
public enum TargetMonths {

    Current(0), // nl
    January(1), // nl
    February(2), // nl
    March(3), // nl
    April(4), // nl
    May(5), // nl
    June(6), // nl
    July(7), // nl
    August(8), // nl
    September(9), // nl
    October(10), // nl
    November(11), // nl
    December(12);

    private Integer value;

    TargetMonths(final Integer value) {
	this.value = value;
    }

    public Integer value() {
	return this.value;
    }
}
