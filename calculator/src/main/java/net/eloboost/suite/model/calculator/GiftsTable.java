package net.eloboost.suite.model.calculator;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import net.eloboost.suite.exceptions.ColumnValueParseException;
import net.eloboost.suite.exceptions.InvalidTableColumnException;

/**
 * Representation of Eloboost 'Gifts' data table.
 * 
 * @author Georgi Iliev
 */
public class GiftsTable extends AbstractDataTable {

    private static final Logger LOGGER = Logger.getLogger(GiftsTable.class);

    private static final String KEY_ORDER_CASH = "orderCash";
    private static final String KEY_ORDER_DATE = "orderDate";
    private static final String КЕY_ORDER_ID = "orderId";

    /**
     * @param page
     *            - the page.
     * @param boost
     *            - the boost.
     */
    public GiftsTable(final Document page, final Boost boost) {
	super(page, boost);
	dataTableContainerElement = "div[id=giftlogs]";
	dataTableElement = "table[class=table table-bordered table-striped table-hover table-center]";
    }

    @Override
    protected void initializeDataTableColumns() {
	dataTableColumns.put(КЕY_ORDER_ID, 0);
	dataTableColumns.put(KEY_ORDER_DATE, 1);
	dataTableColumns.put(KEY_ORDER_CASH, 2);
    }

    @Override
    public double profit() throws InvalidTableColumnException, ColumnValueParseException {
	int dateIndex = dataTableColumns.get(KEY_ORDER_DATE);
	int orderIndex = dataTableColumns.get(КЕY_ORDER_ID);
	int cashIndex = dataTableColumns.get(KEY_ORDER_CASH);

	double giftCash = 0.0d;
	double totalGiftCash = 0.0d;

	DateTime orderDate = null;

	Element orderDateElement = null;
	Element orderIdElement = null;
	Element giftCashElement = null;

	Element giftsTableContainerElement = page.select(dataTableContainerElement).first();
	Element giftsTableElement = giftsTableContainerElement.select(dataTableElement).first();

	Elements rows = giftsTableElement.select("tr");
	for (int i = 1; i < rows.size(); i++) {
	    try {
		orderDateElement = rows.get(i).select("td").get(dateIndex);
		orderIdElement = rows.get(i).select("td").get(orderIndex);

		orderDate = DateTimeFormat.forPattern(ELOBOOST_DATE_PATTERN).parseDateTime(orderDateElement.text());
		orderDate = orderDate.withYear(2000 + orderDate.year().get());

		LOGGER.info("---------- ORDER: " + orderIdElement.text() + "----------");
		LOGGER.info("ORDER DATE: " + orderDate.toString(ELOBOOST_DATE_PATTERN));

		if (boost.isWithinBoostRun(orderDate)) {
		    giftCashElement = rows.get(i).select("td").get(cashIndex);
		    giftCash = Double.parseDouble(giftCashElement.text().replaceAll("[\\s€]", ""));
		    LOGGER.info("GIFT CASH: " + giftCash + " €");

		    totalGiftCash += giftCash;
		} else {
		    LOGGER.info("ORDER NOT WITHIN BOOST RUN!");
		}

		LOGGER.info("---------------------------------------------------------");
	    } catch (UnsupportedOperationException | IllegalArgumentException parseExc) {
		LOGGER.error(parseExc.getMessage(), parseExc);

		String orderDateTrace = getColumnValueTrace(orderDateElement);
		String gifCashTrace = getColumnValueTrace(giftCashElement);

		throw new ColumnValueParseException(orderDateTrace + gifCashTrace, 0);
	    } catch (RuntimeException re) {
		LOGGER.error(re.getMessage(), re);

		String orderIdTrace = getColumnValueTrace(orderIdElement);
		String orderDateTrace = getColumnValueTrace(orderDateElement);
		String gifCashTrace = getColumnValueTrace(giftCashElement);

		throw new InvalidTableColumnException(orderIdTrace + orderDateTrace + gifCashTrace);
	    }
	}

	return totalGiftCash;
    }

}
