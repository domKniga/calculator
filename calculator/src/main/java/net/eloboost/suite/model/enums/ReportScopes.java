package net.eloboost.suite.model.enums;

/**
 * Enumeration for Eloboost profit report scope.
 * 
 * @author Georgi Iliev
 */
public enum ReportScopes {
    Standard, // nl
    Statistical // nl
}
