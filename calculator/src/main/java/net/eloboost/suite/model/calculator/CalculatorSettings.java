package net.eloboost.suite.model.calculator;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import net.eloboost.suite.model.AbstractSettings;
import net.eloboost.suite.utils.EloboostFileService;

/**
 * Representation of an Eloboost calculator configuration.
 * 
 * @author Georgi Iliev
 */
public class CalculatorSettings extends AbstractSettings {

    private static final Logger LOGGER = Logger.getLogger(CalculatorSettings.class);

    public static final String TARGET_DATE_PATTERN = "dd/MM/yyyy";
    public static final String WAKEUP_TIME_PATTERN = "HH:mm:ss";

    public static final String KEY_SOURCE_CURRENCY = "sourceCurrency";
    public static final String KEY_TARGET_CURRENCY = "targetCurrency";
    public static final String KEY_LOOKUP_DIR = "lookupDir";
    public static final String KEY_TARGET_DATE = "targetDate";
    public static final String KEY_WAKEUP_TIME = "wakeUpTime";

    public static final String DEFAULT_SOURCE_CURRENCY = "EUR";
    public static final String DEFAULT_TARGET_CURRENCY = "BGN";
    public static final String DEFAULT_TARGET_DATE = LocalDate.now().toString(TARGET_DATE_PATTERN);
    public static final String DEFAULT_WAKEUP_TIME = LocalDateTime.now().withHourOfDay(9).withMinuteOfHour(0)
	    .withSecondOfMinute(0).toString(WAKEUP_TIME_PATTERN);

    private String sourceCurrency;
    private String targetCurrency;
    private String lookupDir;
    private String attemptsCap;
    private String targetDate;
    private String wakeUpTime;

    /**
     * @param settingsFile
     *            - the settings file.
     */
    public CalculatorSettings(final String settingsFile) {
	super(settingsFile);
    }

    @Override
    public void load() {
	Properties calculatorSetings = new Properties();

	try {
	    
	    try (InputStream calculatorSettingsAsStream = new FileInputStream(
		    Paths.get(EloboostFileService.CONF_FOLDER.toString(), settingsFile).toFile());) {
		calculatorSetings.load(calculatorSettingsAsStream);
	    } finally {
	    }

	    sourceCurrency = calculatorSetings.getProperty(KEY_SOURCE_CURRENCY, DEFAULT_SOURCE_CURRENCY);
	    targetCurrency = calculatorSetings.getProperty(KEY_TARGET_CURRENCY, DEFAULT_TARGET_CURRENCY);
	    targetDate = calculatorSetings.getProperty(KEY_TARGET_DATE, DEFAULT_TARGET_DATE);
	    lookupDir = calculatorSetings.getProperty(KEY_LOOKUP_DIR, EloboostFileService.DESKTOP_FOLDER.toString());
	    wakeUpTime = calculatorSetings.getProperty(KEY_WAKEUP_TIME, DEFAULT_WAKEUP_TIME);

	} catch (IOException ioe) {
	    LOGGER.error("Failed to read calculator configuration: ", ioe);

	    sourceCurrency = DEFAULT_SOURCE_CURRENCY;
	    targetCurrency = DEFAULT_TARGET_CURRENCY;
	    targetDate = DEFAULT_TARGET_DATE;
	    lookupDir = EloboostFileService.DESKTOP_FOLDER.toString();
	    wakeUpTime = DEFAULT_WAKEUP_TIME;
	}
    }

    @Override
    public void fallbackToDefaults(final Properties calculatorSettings) {
	calculatorSettings.putIfAbsent(KEY_SOURCE_CURRENCY, DEFAULT_SOURCE_CURRENCY);
	calculatorSettings.putIfAbsent(KEY_TARGET_CURRENCY, DEFAULT_TARGET_CURRENCY);
	calculatorSettings.putIfAbsent(KEY_TARGET_DATE, DEFAULT_TARGET_DATE);
	calculatorSettings.putIfAbsent(KEY_LOOKUP_DIR, EloboostFileService.DESKTOP_FOLDER.toString());
	calculatorSettings.putIfAbsent(KEY_WAKEUP_TIME, DEFAULT_WAKEUP_TIME);
    }

    public String getSourceCurrency() {
	return this.sourceCurrency;
    }

    public String getTargetCurrency() {
	return this.targetCurrency;
    }

    public String getTargetDate() {
	return this.targetDate;
    }

    public String getLookupDir() {
	return this.lookupDir;
    }

    public String getAttemptsCap() {
	return this.attemptsCap;
    }

    public String getWakeUpTime() {
	return this.wakeUpTime;
    }
}
