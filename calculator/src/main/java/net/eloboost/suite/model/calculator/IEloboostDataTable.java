package net.eloboost.suite.model.calculator;

import net.eloboost.suite.exceptions.ColumnValueParseException;
import net.eloboost.suite.exceptions.InvalidTableColumnException;

/**
 * Blueprint behavior of an Eloboost data table.
 * 
 * @author Georgi Iliev
 */
public interface IEloboostDataTable {

    /**
     * Parses the table content and calculates cash profit.
     * 
     * @return The cash profit from the table.
     * @throws InvalidTableColumnException
     *             If column value can`t be obtained for column index.
     * @throws ColumnValueParseException
     *             If column value can`t be parsed.
     */
    double profit() throws InvalidTableColumnException, ColumnValueParseException;

}
