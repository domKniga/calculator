package net.eloboost.suite.model.calculator;

import java.util.HashMap;
import java.util.Map;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import net.eloboost.suite.exceptions.ColumnValueParseException;
import net.eloboost.suite.exceptions.InvalidTableColumnException;

/**
 * Abstract model of an Eloboost data table.
 * 
 * @author Georgi Iliev
 */
public abstract class AbstractDataTable implements IEloboostDataTable {

    protected static final String ELOBOOST_DATE_PATTERN = "dd/MM/yyyy HH:mm:ss";

    /**
     * HTML container element(i.e. div, form)...
     */
    protected String dataTableContainerElement;

    /**
     * HTML table element.
     */
    protected String dataTableElement;

    /**
     * HTML table header(key)/index(value) pairs.
     */
    protected Map<String, Integer> dataTableColumns;

    /**
     * The parsed(JSoup) Eloboost page object.
     */
    protected Element page;

    protected Boost boost;

    /**
     * @param page
     *            - the page.
     * @param boost
     *            - the boost.
     */
    public AbstractDataTable(final Document page, final Boost boost) {
	this.page = page;
	this.boost = boost;
	dataTableColumns = new HashMap<String, Integer>();
	initializeDataTableColumns();
    }

    /**
     * Creates a report(trace) of a column value. Intended for
     * logging/debugging/maintenance purposes.
     * 
     * @param extractedValueElement
     *            - the element representing the supposed value.
     * @return A report for the column name, index and supposed extracted value.
     */
    protected String getColumnValueTrace(final Element extractedValueElement) {
	if (extractedValueElement == null) {
	    return "No element found!\n";
	}

	StringBuilder valueTraceBuilder = new StringBuilder();

	valueTraceBuilder.append("-----------------------------------------").append("\n");
	for (Map.Entry<String, Integer> column : dataTableColumns.entrySet()) {
	    valueTraceBuilder // nl
		    .append("Column: ").append(column.getKey()).append("\n") // nl
		    .append("Index: ").append(column.getValue()).append("\n") // nl
		    .append("Extracted value: ").append(extractedValueElement.text()).append("\n");
	}
	valueTraceBuilder.append("-----------------------------------------").append("\n");

	return valueTraceBuilder.toString();
    }

    /**
     * Initiliazes the dictionary of relevant data table columns.
     */
    protected abstract void initializeDataTableColumns();

    public abstract double profit() throws InvalidTableColumnException, ColumnValueParseException;

}
