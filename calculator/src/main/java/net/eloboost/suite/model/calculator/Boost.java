package net.eloboost.suite.model.calculator;

import java.util.Objects;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Interval;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

/**
 * Representation of an Eloboost day. Default wake up time is 09:00 AM.
 * 
 * @author Georgi Iliev
 */
public class Boost {

    private static final LocalTime DEFAULT_WAKE_UP_TIME = new LocalTime(9, 0, 0, 0);

    /**
     * Scope: The current day or the day after midnight but before
     * {@code DEFAULT_WAKE_UP_TIME}
     */
    private DateTime boostDay;

    /**
     * Scope: {@code DEFAULT_WAKE_UP_TIME} of current boost day until
     * {@code DEFAULT_WAKE_UP_TIME} of the next day.
     */
    private Interval boostRun;

    /**
     * @param boostDate
     *            - the taget date to use for calculation.
     */
    public Boost(final LocalDate boostDate) {
	this(boostDate, DEFAULT_WAKE_UP_TIME);
    }

    /**
     * @param newBoostDayThreshold
     *            - the hour of day to serve as boost run threshold( Value: 0 -
     *            23 );
     */
    public Boost(final LocalTime newBoostDayThreshold) {
	this(null, newBoostDayThreshold);
    }

    /**
     * @param boostDate
     *            - the taget date to use for calculation.
     * @param newBoostDayThreshold
     */
    public Boost(final LocalDate boostDate, final LocalTime newBoostDayThreshold) {
	this.boostDay = (Objects.equals(LocalDate.now(), boostDate)) // nl
		? determineCurrentBoostDay(newBoostDayThreshold) // nl
		: LocalDateTime.fromDateFields(boostDate.toDate()).toDateTime();

	DateTime boostStart = this.boostDay.withTime(newBoostDayThreshold);
	DateTime boostEnd = this.boostDay.plusDays(1).withTime(newBoostDayThreshold);

	this.boostRun = new Interval(boostStart, boostEnd);
    }

    /**
     * Determines the boost day by matching the current hour of day against the
     * wake up hour - if current hour of day is before the wake up hour of the
     * following day than the previous day is picked(i.e. just after midnight
     * means the day that just passed).
     * 
     * @param newBoostDayThreshold
     *            - the hour of wake up on the following day.
     * @return The current day or the day after midnight but before
     *         {@code DEFAULT_WAKE_UP_TIME}.
     */
    private DateTime determineCurrentBoostDay(final LocalTime newBoostDayThreshold) {
	final DateTime now = DateTime.now();

	int currentHour = now.hourOfDay().get();
	int thresholdHour = newBoostDayThreshold.getHourOfDay();

	return (currentHour > DateTimeConstants.AM && currentHour < thresholdHour) ? now.minusDays(1) : now;

    }

    /**
     * @return Only the boost day`s date part, disregarding time.
     */
    public LocalDate date() {
	return this.boostDay.toLocalDate();
    }

    /**
     * @return The period representing the boost run.
     */
    public Interval boostRun() {
	return this.boostRun;
    }

    /**
     * Determines if the given order is within the boost run.
     * 
     * @param orderDate
     *            - the date and time of an order.
     * @return TRUE if the order fits in the boost run interval, FALSE
     *         otherwise.
     */
    public boolean isWithinBoostRun(final DateTime orderDate) {
	return boostRun.contains(orderDate);
    }
}
