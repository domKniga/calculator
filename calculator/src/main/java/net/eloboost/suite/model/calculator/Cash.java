package net.eloboost.suite.model.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.log4j.Logger;

import net.eloboost.suite.exceptions.ConverterConnectException;
import net.eloboost.suite.exceptions.ConverterTimeoutException;
import net.eloboost.suite.utils.calculator.IEloboostCashConverter;

/**
 * Representation of an Eloboost cash amount(default currency: EUR).
 * 
 * @author Georgi Iliev
 */
public class Cash {

    private static final Logger LOGGER = Logger.getLogger(Cash.class);

    /**
     * The raw(primitive) value - as double.
     */
    private double rawValue;

    /**
     * The finance value - with scale = 2 as standard for figures.
     */
    private BigDecimal financeValue;

    /**
     * The currency standard abbreviation(i.e. USD, EUR)
     */
    private String currency;

    /**
     * The cash converter implementation to use for currency conversions.
     */
    private IEloboostCashConverter converter;

    /**
     * @param currency
     *            - the currency abbreviation.
     */
    public Cash(final String currency) {
	this(0.0d, currency, null);
    }

    /**
     * @param currency
     *            - the currency abbreviation.
     * @param converter
     *            - the converter to be used.
     */
    public Cash(final String currency, final IEloboostCashConverter converter) {
	this(0.0d, currency, converter);
    }

    /**
     * @param rawValue
     *            - the raw value of profits.
     * @param currency
     *            - the currency abbreviation.
     * @param converter
     *            - the converter to be used.
     */
    public Cash(final double rawValue, final String currency, final IEloboostCashConverter converter) {
	this.currency = currency;
	this.converter = converter;

	this.rawValue = rawValue;
	this.financeValue = new BigDecimal(rawValue).setScale(2, RoundingMode.HALF_UP);
    }

    public double getRawValue() {
	return this.rawValue;
    }

    public BigDecimal getFinanceValue() {
	return this.financeValue;
    }

    public String getCurrency() {
	return this.currency;
    }

    /**
     * Attempts to convert this objects raw value to the specified
     * {@code targetCurrency}. In case of failure a fallback mechanism provides
     * roughly calculated value.
     * 
     * @param targetCurrency
     *            - the target currency standard abbreviation(i.e. USD, EUR)
     * @return A cash object representing the conversion product.
     */
    public Cash convertTo(final String targetCurrency) {
	try {

	    return converter.convert(this, targetCurrency);

	} catch (ConverterTimeoutException | ConverterConnectException converterExc) {
	    LOGGER.error(converterExc.getMessage(), converterExc);

	    return converter.convertFallback(this, targetCurrency);
	}
    }

}
