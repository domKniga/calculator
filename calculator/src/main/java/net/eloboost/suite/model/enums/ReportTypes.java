package net.eloboost.suite.model.enums;

/**
 * Enumeration for Eloboost profit report type.
 * 
 * @author Georgi Iliev
 */
public enum ReportTypes {
    Monthly, // nl
    Annual // nl
}
