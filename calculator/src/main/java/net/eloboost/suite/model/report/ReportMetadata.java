package net.eloboost.suite.model.report;

import java.util.Objects;
import java.util.regex.Pattern;

import org.joda.time.LocalDate;
import org.joda.time.YearMonth;

import net.eloboost.suite.model.enums.ReportScopes;
import net.eloboost.suite.model.enums.ReportTypes;
import net.eloboost.suite.model.enums.TargetMonths;

/**
 * The report`s meta data.
 * 
 * @author Georgi Iliev
 */
public class ReportMetadata {

    private String name;
    private Pattern filter;

    /**
     * Processes the report settings and formulates the report`s meta
     * data.Assumes currents for both target month and year.
     * 
     * @param type
     *            - the report type.
     * @param scope
     *            - the report scope.
     */
    public ReportMetadata(final ReportTypes type, final ReportScopes scope) {
	this(type, scope, TargetMonths.Current, LocalDate.now().year().getAsText());
    }

    /**
     * Processes the report settings and formulates the report`s meta data.
     * 
     * @param type
     *            - the report type.
     * @param scope
     *            - the report scope.
     * @param targetMonth
     *            - the specified month.
     * @param targetYear
     *            - the specified year.
     */
    public ReportMetadata(final ReportTypes type, final ReportScopes scope, final TargetMonths targetMonth,
	    final String targetYear) {
	LocalDate today = LocalDate.now();

	String month = (Objects.equals(targetMonth, TargetMonths.Current)) // nl
		? today.monthOfYear().getAsText() // nl
		: new YearMonth(today.getYear(), targetMonth.value()).monthOfYear().getAsText();
	String year = targetYear;

	String pattern = null;
	switch (type) {
	case Monthly:
	    this.name = scope.equals(ReportScopes.Statistical) // nl
		    ? ("[ALLYEARS]" + month) // nl
		    : ("[" + year + "]" + month);

	    pattern = scope.equals(ReportScopes.Statistical) // nl
		    ? (Pattern.quote("[") + "\\d\\d\\d\\d" + Pattern.quote("]") + month + ".txt\\z") // nl
		    : (Pattern.quote("[" + year + "]") + month + ".txt\\z");

	    break;
	case Annual:
	    this.name = scope.equals(ReportScopes.Statistical) // nl
		    ? ("[ALLTIME]") // nl
		    : ("[" + year + "]");

	    pattern = scope.equals(ReportScopes.Statistical) // nl
		    ? null// nl
		    : (Pattern.quote("[" + year + "]") + "\\w+.txt\\z");

	    break;
	default:
	    break;
	}

	this.filter = (pattern == null) ? null : Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
    }

    public String getReportName() {
	return this.name;
    }

    public Pattern getFilter() {
	return this.filter;
    }

    @Override
    public String toString() {
	StringBuilder summaryBuilder = new StringBuilder();

	summaryBuilder.append("Report metadata summary:") // nl
		.append("\n------------------------") // nl
		.append("\n Report name: " + this.name) // nl
		.append("\n Filter(RegExp): " + this.filter);

	return summaryBuilder.toString();
    }
}
