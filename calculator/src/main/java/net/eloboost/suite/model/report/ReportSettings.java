package net.eloboost.suite.model.report;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.joda.time.LocalDate;

import net.eloboost.suite.model.AbstractSettings;
import net.eloboost.suite.model.enums.ReportScopes;
import net.eloboost.suite.model.enums.ReportTypes;
import net.eloboost.suite.model.enums.TargetMonths;
import net.eloboost.suite.utils.EloboostFileService;

/**
 * Representation of an Eloboost report settings.
 * 
 * @author Georgi Iliev
 */
public class ReportSettings extends AbstractSettings {

    private static final Logger LOGGER = Logger.getLogger(ReportSettings.class);

    public static final String TARGET_YEAR_PATTERN = "yyyy";

    public static final String KEY_TYPE = "type";
    public static final String KEY_SCOPE = "scope";
    public static final String KEY_TARGET_MONTH = "targetMonth";
    public static final String KEY_TARGET_YEAR = "targetYear";
    public static final String KEY_OUTPUT_DIR = "outputDir";

    public static final String DEFAULT_REPORT_TYPE = ReportTypes.Monthly.toString();
    public static final String DEFAULT_REPORT_SCOPE = ReportScopes.Standard.toString();
    public static final String DEFAULT_TARGET_MONTH = TargetMonths.Current.toString();
    public static final String DEFAULT_TARGET_YEAR = LocalDate.now().year().getAsText();

    private String type;
    private String scope;
    private String targetMonth;
    private String targetYear;
    private String outputDir;

    /**
     * @param settingsFile
     *            - the settings file.
     */
    public ReportSettings(final String settingsFile) {
	super(settingsFile);
    }

    @Override
    public void load() {
	Properties reportSettings = new Properties();

	try {

	    try (InputStream reportSettingsAsStream = new FileInputStream(
		    Paths.get(EloboostFileService.CONF_FOLDER.toString(), settingsFile).toFile());) {
		reportSettings.load(reportSettingsAsStream);
	    } finally {
	    }

	    type = reportSettings.getProperty(KEY_TYPE, DEFAULT_REPORT_TYPE);
	    scope = reportSettings.getProperty(KEY_SCOPE, DEFAULT_REPORT_SCOPE);
	    targetMonth = reportSettings.getProperty(KEY_TARGET_MONTH, DEFAULT_TARGET_MONTH);
	    targetYear = reportSettings.getProperty(KEY_TARGET_YEAR, DEFAULT_TARGET_YEAR);
	    outputDir = reportSettings.getProperty(KEY_OUTPUT_DIR, EloboostFileService.REPORTS_FOLDER.toString());

	} catch (IOException ioe) {
	    LOGGER.error("Failed to read configuration(loaded default configuration): ", ioe);

	    type = DEFAULT_REPORT_TYPE;
	    scope = DEFAULT_REPORT_SCOPE;
	    targetMonth = DEFAULT_TARGET_MONTH;
	    targetYear = DEFAULT_TARGET_YEAR;
	    outputDir = EloboostFileService.REPORTS_FOLDER.toString();
	}
    }

    @Override
    public void fallbackToDefaults(final Properties reportSettings) {
	reportSettings.putIfAbsent(KEY_TYPE, DEFAULT_REPORT_TYPE);
	reportSettings.putIfAbsent(KEY_SCOPE, DEFAULT_REPORT_SCOPE);
	reportSettings.putIfAbsent(KEY_TARGET_MONTH, DEFAULT_TARGET_MONTH);
	reportSettings.putIfAbsent(KEY_TARGET_YEAR, DEFAULT_TARGET_YEAR);
	reportSettings.putIfAbsent(KEY_OUTPUT_DIR, EloboostFileService.REPORTS_FOLDER.toString());
    }

    public String getType() {
	return this.type;
    }

    public String getScope() {
	return this.scope;
    }

    public String getTargetMonth() {
	return this.targetMonth;
    }

    public String getTargetYear() {
	return this.targetYear;
    }

    public String getOutputDir() {
	return this.outputDir;
    }
}
