package net.eloboost.suite.utils.calculator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;

import net.eloboost.suite.exceptions.ConverterConnectException;
import net.eloboost.suite.exceptions.ConverterTimeoutException;
import net.eloboost.suite.model.calculator.Cash;

/**
 * Cash converter utilizing the Yahoo Finance API.
 * 
 * @author Georgi Iliev
 */
public class YahooCashConverter extends AbstractCashConverter {

    private static final Logger LOGGER = Logger.getLogger(YahooCashConverter.class);

    public YahooCashConverter(final int maximumRetryAttempts) {
	super(maximumRetryAttempts);
    }

    @Override
    protected void initializeFallbackRatios() {
	fallbackRatios.put("EUR=>BGN", 1.95d);
	fallbackRatios.put("USD=>BGN", 1.74d);
	fallbackRatios.put("BGN=>EUR", -1.95d);
	fallbackRatios.put("BGN=>USD", -1.74d);
    }

    @Override
    public Cash convert(final Cash cashToConvert, final String targetCurrency)
	    throws ConverterConnectException, ConverterTimeoutException {
	double originalValue = cashToConvert.getRawValue();
	String originalCurrency = cashToConvert.getCurrency();

	String conversion = originalCurrency + "=>" + targetCurrency;

	URL url;
	try {
	    url = new URL("http://finance.yahoo.com/d/quotes.csv?f=l1&s=" + originalCurrency + targetCurrency + "=X");
	} catch (MalformedURLException mue) {
	    throw new ConverterConnectException(
		    "Could not connect to cash conversion service! Conversion: " + conversion, mue);
	}

	for (int i = 1; i <= retryAttemptsCap; i++) {
	    try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));) {

		String convertedValueAsString = reader.readLine();
		if (convertedValueAsString.length() > 0) {
		    double convertedValue = Double.parseDouble(convertedValueAsString) * originalValue;

		    return new Cash(convertedValue, targetCurrency, this);
		}
	    } catch (IOException ioe) {
		continue;
	    } finally {
		LOGGER.info("[ATTEMPT #" + i + "] Org.Value: " + originalValue + "; Conversion: " + conversion);
	    }
	}

	throw new ConverterTimeoutException(
		"Conversion:" + conversion + " failed in " + retryAttemptsCap + " attempts!");
    }

}
