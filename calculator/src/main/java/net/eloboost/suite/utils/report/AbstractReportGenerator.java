package net.eloboost.suite.utils.report;

import java.io.IOException;
import java.nio.file.Path;

import net.eloboost.suite.model.report.ReportMetadata;

/**
 * Abstract model of an Eloboost profit report generator.
 * 
 * @author Georgi Iliev
 */
public abstract class AbstractReportGenerator implements IEloboostReportGenerator {

    protected ReportMetadata metadata;

    /**
     * @param metadata
     *            - the report meta data.
     */
    public AbstractReportGenerator(final ReportMetadata metadata) {
	this.metadata = metadata;
    }

    public abstract void generateReport(final Path outputDir) throws IOException;
}
