package net.eloboost.suite.utils;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.joda.time.LocalDate;

import net.eloboost.suite.exceptions.LookupFolderNotFoundException;

/**
 * Utility for common file related(I/O) operations.
 * 
 * @author Georgi Iliev
 */
public class EloboostFileService {

    private static final Logger LOGGER = Logger.getLogger(EloboostFileService.class);

    public static final Charset DEFAULT_ENCODING = StandardCharsets.UTF_8;

    public static final Path DESKTOP_FOLDER = Paths.get(System.getenv("USERPROFILE"), "Desktop");
    public static final Path PARENT_FOLDER = Paths.get(System.getProperty("user.dir"));
    public static final Path CONF_FOLDER = Paths.get(PARENT_FOLDER.toString(), "conf");
    public static final Path IMG_FOLDER = Paths.get(PARENT_FOLDER.toString(), "images");
    public static final Path OUTPUT_FOLDER = Paths.get(PARENT_FOLDER.toString(), "out");
    public static final Path LOGS_FOLDER = Paths.get(PARENT_FOLDER.toString(), "logs");
    public static final Path LEDGERS_FOLDER = Paths.get(OUTPUT_FOLDER.toString(), "ledgers");
    public static final Path REPORTS_FOLDER = Paths.get(OUTPUT_FOLDER.toString(), "reports");

    private EloboostFileService() {
    }

    private static String[] filter(final File lookupFolder, final CustomFileFilter filter) {
	String[] filteredFiles = lookupFolder.list(filter);
	if (filteredFiles == null) {
	    throw new LookupFolderNotFoundException(lookupFolder);
	}

	return filteredFiles;
    }

    /**
     * @throws IOException
     *             In case of an I/O error.
     */
    public static void tryCreateApplicationDirs() throws IOException {
	Files.createDirectories(IMG_FOLDER);
	Files.createDirectories(CONF_FOLDER);
	Files.createDirectories(OUTPUT_FOLDER);
	Files.createDirectories(LOGS_FOLDER);
	Files.createDirectories(LEDGERS_FOLDER);
	Files.createDirectories(REPORTS_FOLDER);
    }

    /**
     * Loads the contents of each Eloboost page found in a specified lookup
     * folder.
     * 
     * @param lookupDir
     *            - the lookup folder.
     * @return A collection of unique Eloboost page contents.
     * @throws IOException
     *             In case of an I/O error.
     */
    public static Set<String> loadEloboostPages(final Path lookupDir) throws IOException {
	Set<String> eloboostPages = new HashSet<>();

	File lookupFolder = new File(lookupDir.toString());
	CustomFileFilter htmlFilter = new CustomFileFilter(".html");

	String[] availablePages = filter(lookupFolder, htmlFilter);
	for (String pageName : availablePages) {
	    if (!pageName.toLowerCase().contains("elo-boost.net")) {
		continue;
	    }

	    Path pagePath = Paths.get(lookupDir.toString(), pageName);
	    String pageContent = getFileContentAsUTF8String(pagePath);

	    boolean pageAdded = eloboostPages.add(pageContent);
	    if (pageAdded) {
		LOGGER.info("Added page:[" + pageName + "]");
	    } else {
		LOGGER.info("Skipping duplicate page:[" + pageName + "]");
	    }

	    Files.delete(pagePath);
	}

	return eloboostPages;
    }

    /**
     * Logs the cash profit made during the boost date(boost run).
     * 
     * @param boostDate
     *            - the boost date(boost run).
     * @param convertedCash
     *            - the ORIGINAL finance value, representing the profit from the
     *            boost date(boost run).
     * @param convertedCash
     *            - the CONVERTED finance value, representing the profit from
     *            the boost date(boost run).
     * @throws IOException
     *             In case of an I/O error.
     */
    public static void appendLedger(final LocalDate boostDate, final BigDecimal originalCash,
	    final BigDecimal convertedCash) throws IOException {
	String ledgerName = "[" + boostDate.getYear() + "]" + boostDate.monthOfYear().getAsText() + ".txt";
	Path ledgerPath = Paths.get(EloboostFileService.LEDGERS_FOLDER.toString(), ledgerName);
	List<String> entry = Collections.singletonList(boostDate + "," + originalCash + "," + convertedCash);

	Files.write(ledgerPath, entry, DEFAULT_ENCODING, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
    }

    /**
     * Filters the ledgers via a regular expression pattern.
     * 
     * @param ledgerFilterPattern
     *            - the ledger file name pattern.
     * @return A list of filtered ledger file names.
     * @throws IOException
     *             If the folder is not found or there is a problem reading its
     *             files.
     */
    public static List<String> filterLedgers(final Pattern ledgerFilterPattern) throws IOException {
	List<String> filteredLedgers = new ArrayList<>();

	CustomFileFilter txtFilter = new CustomFileFilter(".txt");
	File lookupFolder = new File(LEDGERS_FOLDER.toString());

	List<String> ledgers = Arrays.asList(filter(lookupFolder, txtFilter));
	if (ledgerFilterPattern == null) {
	    filteredLedgers.addAll(ledgers);
	} else {
	    for (String ledgerName : ledgers) {
		if (ledgerFilterPattern.matcher(ledgerName).matches()) {
		    filteredLedgers.add(ledgerName);
		}
	    }
	}

	return filteredLedgers;
    }

    /**
     * @param filePath
     *            - the file path.
     * @return The contents of the file as an UTF-8 string.
     * @throws IOException
     *             If there is a problem reading the file.
     */
    public static String getFileContentAsUTF8String(final Path filePath) throws IOException {
	File file = filePath.toFile();
	byte[] rawContent = Files.readAllBytes(Paths.get(file.getPath()));

	return new String(rawContent, DEFAULT_ENCODING);
    }
}
