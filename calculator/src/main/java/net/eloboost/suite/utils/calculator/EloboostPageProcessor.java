package net.eloboost.suite.utils.calculator;

import org.apache.log4j.Logger;
import org.joda.time.Interval;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import net.eloboost.suite.exceptions.ColumnValueParseException;
import net.eloboost.suite.exceptions.InvalidTableColumnException;
import net.eloboost.suite.model.calculator.Boost;
import net.eloboost.suite.model.calculator.GiftsTable;
import net.eloboost.suite.model.calculator.IEloboostDataTable;
import net.eloboost.suite.model.calculator.OrdersTable;

/**
 * EloboostPageProcessor - responsible for parsing data tables and extracting
 * total boost run cash raw value.
 * 
 * @author Georgi Iliev
 */
public class EloboostPageProcessor implements IEloboostPageProcessor {

    private static final Logger LOGGER = Logger.getLogger(EloboostPageProcessor.class);

    private String pageContent;
    private Boost boostDay;

    public EloboostPageProcessor(final String pageContent, final Boost boost) {
	this.pageContent = pageContent;
	this.boostDay = boost;
    }

    @Override
    public double processEloboostPage() {
	double cashFromGifts = 0.0d;
	double cashFromOrders = 0.0d;

	Interval boostRun = boostDay.boostRun();

	LOGGER.info("---------------------------------------------------------");
	LOGGER.info("BOOST START: " + boostRun.getStart().toString("dd/MM/yyyy HH:mm:ss"));
	LOGGER.info("BOOST END: " + boostRun.getEnd().toString("dd/MM/yyyy HH:mm:ss"));

	Document page = Jsoup.parse(pageContent);

	IEloboostDataTable giftsDataTable = new GiftsTable(page, boostDay);
	IEloboostDataTable ordersDataTable = new OrdersTable(page, boostDay);

	try {
	    cashFromGifts = giftsDataTable.profit();
	    cashFromOrders = ordersDataTable.profit();

	    return cashFromGifts + cashFromOrders;
	} catch (InvalidTableColumnException | ColumnValueParseException columnValueExc) {
	    LOGGER.error(columnValueExc.getMessage(), columnValueExc);

	    throw new RuntimeException("Processing profit from data table failed!", columnValueExc);
	}
    }

}
