package net.eloboost.suite.utils.report;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Blueprint behavior of an Eloboost profit report generator.
 * 
 * @author Georgi Iliev
 */
public interface IEloboostReportGenerator {

    /**
     * Generates an Eloboost profit report using the metadata passed during
     * generator construction.
     * 
     * @param outputDir
     *            - folder to output the generated report in.
     * @throws IOException
     */
    void generateReport(final Path outputDir) throws IOException;
}
