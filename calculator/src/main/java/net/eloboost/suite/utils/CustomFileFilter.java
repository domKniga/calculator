package net.eloboost.suite.utils;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Filename filter implementation for custom file searches.
 * 
 * @author Georgi Iliev
 */
public class CustomFileFilter implements FilenameFilter {

    private String extension;

    /**
     * @param extension
     *            - the allowed file extension.
     */
    public CustomFileFilter(final String extension) {
	this.extension = extension;
    }

    @Override
    public boolean accept(File dir, String name) {
	return (name.endsWith(extension));
    }

}
