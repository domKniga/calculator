package net.eloboost.suite.utils.report;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.DateFormat;
import jxl.write.DateTime;
import jxl.write.Formula;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.NumberFormats;
import jxl.write.WritableCell;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import net.eloboost.suite.model.report.ReportMetadata;
import net.eloboost.suite.utils.EloboostFileService;

/**
 * Excell profit report generator.
 * 
 * @author Georgi Iliev
 */
public class ExcellReportGenerator extends AbstractReportGenerator {

    private static final Logger LOGGER = Logger.getLogger(ExcellReportGenerator.class);

    private static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";

    private static DateFormat dateValueFormat;
    private static WritableCellFormat headerCellFormat;
    private static WritableCellFormat textCellFormat;
    private static WritableCellFormat dateCellFormat;
    private static WritableCellFormat currencyCellFormat;
    private static WritableCellFormat bottomlineTextCellFormat;
    private static WritableCellFormat bottomlineCurrencyCellFormat;

    /**
     * @param reportMetadata
     *            - the report meta data.
     */
    public ExcellReportGenerator(final ReportMetadata reportMetadata) {
	super(reportMetadata);
    }

    @Override
    public void generateReport(final Path outputDir) throws IOException {
	try {
	    Pattern ledgerFilterPattern = metadata.getFilter();

	    List<String> ledgers = EloboostFileService.filterLedgers(ledgerFilterPattern);
	    if (ledgers.isEmpty()) {
		JOptionPane.showMessageDialog(null,
			"Report generation skipped. No ledger matches your criteria:\n" + metadata.toString(),
			"WARNING", JOptionPane.WARNING_MESSAGE);
		return;
	    }

	    File workbookFile = Paths.get(outputDir.toString(), metadata.getReportName() + ".xls").toFile();
	    WorkbookSettings workbookSettings = initializeWorkbookSettings(EloboostFileService.DEFAULT_ENCODING, false);

	    WritableWorkbook profitReporWorkbook = Workbook.createWorkbook(workbookFile, workbookSettings);

	    initializeStyles();
	    createSheets(ledgers, profitReporWorkbook);
	    createSummarySheet(profitReporWorkbook);

	    profitReporWorkbook.write();
	    profitReporWorkbook.close();

	    JOptionPane.showMessageDialog(null, "Report generated: " + workbookFile.getName(), "DONE",
		    JOptionPane.INFORMATION_MESSAGE);
	    Desktop.getDesktop().open(outputDir.toFile());

	} catch (WriteException | ParseException excellException) {
	    String reportGenFailedMsg = "Excell report generation failed! ";
	    LOGGER.error(reportGenFailedMsg + excellException.getMessage(), excellException);

	    throw new IOException(reportGenFailedMsg, excellException);
	}
    }

    private void createSheets(final List<String> ledgers, final WritableWorkbook profitReporWorkbook)
	    throws WriteException, RowsExceededException, IOException, ParseException {
	for (int ledgerIndex = 0, sheetNumber = 1; ledgerIndex < ledgers.size(); ledgerIndex++, sheetNumber++) {
	    String ledgerName = ledgers.get(ledgerIndex);
	    String sheetName = ledgerName.replace("[", "(").replace("]", ")").replace(".txt", "");

	    WritableSheet profitReportSheet = profitReporWorkbook.createSheet(sheetName, sheetNumber);
	    initializeSheetSettings(profitReportSheet, false);

	    String[] headers = new String[] { "Date", "Profit(EUR)", "Profit(BGN)" };
	    addHeaders(profitReportSheet, headers);

	    Path ledgerFilePath = Paths.get(EloboostFileService.LEDGERS_FOLDER.toString(), ledgerName);
	    String ledgerContentAsString = EloboostFileService.getFileContentAsUTF8String(ledgerFilePath);
	    String ledgerEntries[] = ledgerContentAsString.split(System.getProperty("line.separator"));

	    for (int entryIndex = 0, rowNumber = 1; entryIndex < ledgerEntries.length; entryIndex++, rowNumber++) {
		String ledgerEntryValues[] = ledgerEntries[entryIndex].split(",");

		Date boostDayValue = dateValueFormat.getDateFormat().parse(ledgerEntryValues[0]);
		DateTime boostDayCell = new DateTime(0, rowNumber, boostDayValue, dateCellFormat);
		profitReportSheet.addCell(boostDayCell);

		Double originalProfitValue = Double.valueOf(ledgerEntryValues[1]);
		Number originalProfitCell = new Number(1, rowNumber, originalProfitValue, currencyCellFormat);
		profitReportSheet.addCell(originalProfitCell);

		Double targetProfitValue = Double.valueOf(ledgerEntryValues[2]);
		Number targetProfitCell = new Number(2, rowNumber, targetProfitValue, currencyCellFormat);
		profitReportSheet.addCell(targetProfitCell);
	    }
	    addBottomline(profitReportSheet);

	}
    }

    private void createSummarySheet(final WritableWorkbook profitReporWorkbook)
	    throws WriteException, RowsExceededException {
	WritableSheet[] profitReportSheets = profitReporWorkbook.getSheets();

	int sheetNumber = profitReporWorkbook.getNumberOfSheets() + 1;

	WritableSheet summarySheet = profitReporWorkbook.createSheet("SUMMARY", sheetNumber);
	initializeSheetSettings(summarySheet, true);

	String[] headers = new String[] { "Sheet", "Profit(EUR)", "Profit(BGN)" };
	addHeaders(summarySheet, headers);

	for (int sheetIndex = 0, rowNumber = 1; sheetIndex < profitReportSheets.length; sheetIndex++, rowNumber++) {

	    WritableSheet profitReportSheet = profitReportSheets[sheetIndex];

	    String sheetName = profitReportSheet.getName();

	    double totalOriginalProfitFromSheet = 0.0d;
	    double totalTargetProfitFromSheet = 0.0d;
	    int rowsExceptBottomline = profitReportSheet.getRows() - 1;

	    for (int profitCellIndex = 1; profitCellIndex < rowsExceptBottomline; profitCellIndex++) {
		Number originalProfitCell = (Number) profitReportSheet.getWritableCell(1, profitCellIndex);
		Number targetProfitCell = (Number) profitReportSheet.getWritableCell(2, profitCellIndex);

		totalOriginalProfitFromSheet += originalProfitCell.getValue();
		totalTargetProfitFromSheet += targetProfitCell.getValue();
	    }

	    Label sheetNameLabel = new Label(0, rowNumber, sheetName, textCellFormat);
	    summarySheet.addCell(sheetNameLabel);

	    Number totalOriginalProfitCell = new Number(1, rowNumber, totalOriginalProfitFromSheet, currencyCellFormat);
	    summarySheet.addCell(totalOriginalProfitCell);

	    Number totalTargetProfitCell = new Number(2, rowNumber, totalTargetProfitFromSheet, currencyCellFormat);
	    summarySheet.addCell(totalTargetProfitCell);
	}

	addBottomline(summarySheet);
    }

    private void addHeaders(final WritableSheet sheet, final String[] headers)
	    throws WriteException, RowsExceededException {
	for (int i = 0; i < headers.length; i++) {
	    Label headerLabel = new Label(i, 0, headers[i]);
	    sheet.addCell(headerLabel);

	    WritableCell headerCell = sheet.getWritableCell(i, 0);
	    headerCell.setCellFormat(headerCellFormat);
	}
    }

    private void addBottomline(final WritableSheet sheet) throws WriteException {
	int lastRow = sheet.getRows();

	String bottomlineLabelText = "TOTAL:";
	Label bottomlineLabel = new Label(0, lastRow, bottomlineLabelText, bottomlineTextCellFormat);
	sheet.addCell(bottomlineLabel);

	String bottomlineFormula = "SUM(B2:B" + lastRow + ")";

	Formula originalBottomlineCell = new Formula(1, lastRow, bottomlineFormula, bottomlineCurrencyCellFormat);
	sheet.addCell(originalBottomlineCell);

	Formula targetBottomlineCell = new Formula(2, lastRow, bottomlineFormula, bottomlineCurrencyCellFormat);
	sheet.addCell(targetBottomlineCell);
    }

    private void initializeStyles() throws WriteException {
	dateValueFormat = new DateFormat(DEFAULT_DATE_PATTERN);

	WritableFont font = new WritableFont(WritableFont.TIMES, 12);
	font.setItalic(true);

	Colour headerColour = Colour.LAVENDER;
	Colour dataColour = Colour.IVORY;
	Colour bottomlineColour = Colour.GOLD;

	textCellFormat = new WritableCellFormat(font);
	textCellFormat.setBackground(dataColour);
	textCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
	textCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
	textCellFormat.setAlignment(Alignment.CENTRE);
	textCellFormat.setShrinkToFit(true);
	textCellFormat.setWrap(true);

	headerCellFormat = new WritableCellFormat(textCellFormat);
	headerCellFormat.setBackground(headerColour);

	dateCellFormat = new WritableCellFormat(font, dateValueFormat);
	dateCellFormat.setBackground(dataColour);
	dateCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
	dateCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
	dateCellFormat.setAlignment(Alignment.CENTRE);
	dateCellFormat.setShrinkToFit(true);
	dateCellFormat.setWrap(true);

	currencyCellFormat = new WritableCellFormat(font, NumberFormats.FLOAT);
	currencyCellFormat.setBackground(dataColour);
	currencyCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
	currencyCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
	currencyCellFormat.setAlignment(Alignment.CENTRE);
	currencyCellFormat.setShrinkToFit(true);
	currencyCellFormat.setWrap(true);

	bottomlineTextCellFormat = new WritableCellFormat(textCellFormat);
	bottomlineTextCellFormat.setBackground(bottomlineColour);
	bottomlineTextCellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);

	bottomlineCurrencyCellFormat = new WritableCellFormat(currencyCellFormat);
	bottomlineCurrencyCellFormat.setBackground(bottomlineColour);
	bottomlineCurrencyCellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
    }

    private WorkbookSettings initializeWorkbookSettings(final Charset encoding, final boolean disableCellValidation) {
	WorkbookSettings workbookSettings = new WorkbookSettings();
	workbookSettings.setEncoding(encoding.toString());
	workbookSettings.setCellValidationDisabled(disableCellValidation);
	workbookSettings.setSuppressWarnings(disableCellValidation);

	return workbookSettings;
    }

    private void initializeSheetSettings(final WritableSheet sheet, final boolean selectByDefault) {
	sheet.getSettings().setSelected(selectByDefault);
	sheet.getSettings().setDefaultColumnWidth(30);
	sheet.getSettings().setProtected(true);
    }

}
