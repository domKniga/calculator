package net.eloboost.suite.utils.calculator;

import net.eloboost.suite.exceptions.ConverterConnectException;
import net.eloboost.suite.exceptions.ConverterTimeoutException;
import net.eloboost.suite.model.calculator.Cash;

/**
 * Blueprint behaviour of an Eloboost cash converter.
 * 
 * @author Georgi Iliev
 */
public interface IEloboostCashConverter {

    /**
     * Performs currency conversion.
     * 
     * @param cashToConvert
     *            - the Eloboost cash to convert.
     * @param targetCurrency
     *            - the target currency standard abbreviation(i.e. USD, EUR).
     * @return A cash object representing the conversion product.
     * @throws ConverterConnectException
     *             In case of malformed request to finance API.
     * @throws ConverterTimeoutException
     *             In case of timeout beyond configured maximum retry attempts.
     */
    Cash convert(final Cash cashToConvert, final String targetCurrency)
	    throws ConverterConnectException, ConverterTimeoutException;

    /**
     * Performs rough(estimated) currency conversion in case the finance API
     * fails.
     * 
     * @param cashToConvert
     *            - the Eloboost cash to convert.
     * @param targetCurrency
     *            - the target currency standard abbreviation(i.e. USD, EUR).
     * @return A cash object representing the conversion product.
     */
    Cash convertFallback(final Cash cashToConvert, final String targetCurrency);
}
