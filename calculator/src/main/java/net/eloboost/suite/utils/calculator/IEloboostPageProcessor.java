package net.eloboost.suite.utils.calculator;

/**
 * Blueprint behavior of an Eloboost page processor.
 * 
 * @author Georgi Iliev
 */
public interface IEloboostPageProcessor {

    /**
     * Processes through the Eloboost page calculating the raw monetary profit
     * for the boost run.
     * 
     * @return The raw cash profit calculated from the Eloboost page.
     */
    double processEloboostPage();
}
