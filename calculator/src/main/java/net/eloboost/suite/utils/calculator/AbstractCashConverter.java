package net.eloboost.suite.utils.calculator;

import java.util.HashMap;
import java.util.Map;

import net.eloboost.suite.exceptions.ConverterConnectException;
import net.eloboost.suite.exceptions.ConverterTimeoutException;
import net.eloboost.suite.model.calculator.Cash;

/**
 * Abstract model of an Eloboost cash converter.
 * 
 * @author Georgi Iliev
 */
public abstract class AbstractCashConverter implements IEloboostCashConverter {

    public static final int DEFAULT_ATTEMPTS_CAP = 20;

    /**
     * Maximum retries of finance API calls.
     */
    protected int retryAttemptsCap;

    /**
     * Key = the conversion(formatted like <i>originalCurrencyAbbreviation =>
     * targetCurrencyAbbreviation }</i>) <br>
     * Value = the fallback ratio </br>
     */
    protected Map<String, Double> fallbackRatios;

    public AbstractCashConverter(final int maximumRetryAttempts) {
	this.retryAttemptsCap = maximumRetryAttempts;
	fallbackRatios = new HashMap<String, Double>();
	initializeFallbackRatios();
    }

    /**
     * Initializes the fallback ratios(At the moment fallback ratios are
     * provided for currencies - EUR,BGN only)
     */
    protected abstract void initializeFallbackRatios();

    public abstract Cash convert(final Cash cashToConvert, final String targetCurrency)
	    throws ConverterConnectException, ConverterTimeoutException;

    public Cash convertFallback(Cash cashToConvert, String targetCurrency) {
	if (cashToConvert == null || ("".equals(targetCurrency) || null == targetCurrency)) {
	    throw new IllegalArgumentException("Illegal/missing target currency or cash object");
	}

	String originalCurrency = cashToConvert.getCurrency();
	String conversion = originalCurrency + "=>" + targetCurrency;
	Double fallbackRatio = fallbackRatios.get(conversion);

	if (fallbackRatio == null) {
	    throw new RuntimeException("No fallback ratio defined for conversion: " + conversion);
	}

	if (fallbackRatio > 0d) {
	    return new Cash(cashToConvert.getRawValue() * Math.abs(fallbackRatio), targetCurrency, this);
	} else {
	    return new Cash(cashToConvert.getRawValue() / Math.abs(fallbackRatio), targetCurrency, this);
	}
    }
}
