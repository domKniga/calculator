package net.eloboost.suite.exceptions;

import java.text.ParseException;

/**
 * Exception signaling invalid format of parseable column value(could mean changes to HTML tags).
 * 
 * @author Georgi Iliev
 */
public class ColumnValueParseException extends ParseException {

    /**
     * Original state
     */
    private static final long serialVersionUID = 3946595802203316336L;

    /**
     * @param message - the message describing the exception.
     * @param errorOffset - the position of parse exception.
     */
    public ColumnValueParseException(String message, int errorOffset) {
	super(message, errorOffset);
    }

}
