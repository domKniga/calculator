package net.eloboost.suite.exceptions;

import java.io.File;

/**
 * Exception signaling that the specified folder for lookup of ledgers was not
 * found in the file system.
 * 
 * @author Georgi Iliev
 */
public class LookupFolderNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1758026383030429161L;

    /**
     * @param lookupFolder
     *            - the specified lookup folder
     */
    public LookupFolderNotFoundException(File lookupFolder) {
	super("Specified lookup folder was not found: " + lookupFolder.getAbsolutePath());
    }

}
