package net.eloboost.suite.exceptions;

import java.awt.Dimension;

public class DisplaySizeNotSupportedException extends RuntimeException {

    private static final long serialVersionUID = 2028015396501288139L;

    /**
     * @param displaySize
     *            - the user`s display size.
     */
    public DisplaySizeNotSupportedException(Dimension displaySize) {
	super("Your display`s size(" + displaySize.width + "x" + displaySize.height + ") is currently not supported!");
    }
}
