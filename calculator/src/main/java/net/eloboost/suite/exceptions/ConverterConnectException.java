package net.eloboost.suite.exceptions;

/**
 * Exception signaling bad request/invalid connection to cash converter service
 * API.
 * 
 * @author Georgi Iliev
 */
public class ConverterConnectException extends RuntimeException {

    /**
     * Original state
     */
    private static final long serialVersionUID = 4996302254939918761L;

    /**
     * @param message
     *            - the error message.
     */
    public ConverterConnectException(String message) {
	super(message);
    }

    /**
     * @param message
     *            - the error message.
     * @param cause
     *            - the cause of the exception.
     */
    public ConverterConnectException(String message, Throwable cause) {
	super(message, cause);
    }

}
