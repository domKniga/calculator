package net.eloboost.suite.exceptions;

/**
 * Exception signaling invalid(missing) column in a Eloboost data table.
 * 
 * @author Georgi Iliev
 */
public class InvalidTableColumnException extends Exception {

    /**
     * Original state.
     */
    private static final long serialVersionUID = 7919144572730074600L;
    
    /**
     * @param message - the error message.
     */
    public InvalidTableColumnException(String message) {
	super(message);
    }
    
}
