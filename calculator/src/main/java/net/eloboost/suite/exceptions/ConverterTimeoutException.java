package net.eloboost.suite.exceptions;

import java.util.concurrent.TimeoutException;

/**
 * Exception signaling failure/timeout of cash converter service.
 * 
 * @author Georgi Iliev
 */
public class ConverterTimeoutException extends TimeoutException {
    
    /**
     * Original state
     */
    private static final long serialVersionUID = -5945400169830949776L;

    /**
     * @param message - the error message.
     */
    public ConverterTimeoutException(String message) {
	super(message);
    }

}
