package net.eloboost.suite;

import java.awt.EventQueue;
import java.nio.file.Paths;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import net.eloboost.suite.utils.EloboostFileService;
import net.eloboost.suite.view.EloboostSuiteUIThread;

/**
 * Entry point of Eloboost calculator execution.
 * 
 * @author Georgi Iliev
 */
public class EloboostCalculatorMain {

    private static final Logger LOGGER = Logger.getLogger(EloboostCalculatorMain.class);

    public static void main(String[] args) {
	try {
	    EloboostFileService.tryCreateApplicationDirs();

	    PropertyConfigurator
		    .configure(Paths.get(EloboostFileService.CONF_FOLDER.toString(), "log4j.properties").toString());

	    EloboostSuiteUIThread uiThread = new EloboostSuiteUIThread();
	    EventQueue.invokeLater(uiThread);

	} catch (Exception e) {
	    LOGGER.error(e.getMessage(), e);

	    JOptionPane.showMessageDialog(null, "Unexpected(fatal) error occurred! Please, contact developer.", "ERROR",
		    JOptionPane.ERROR_MESSAGE);
	}
    }
}
